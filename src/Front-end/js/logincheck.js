$(function () {
//Checked if user is admin
    if (sessionStorage.getItem("admin") === "1") {
        document.getElementById("admin-hidden").classList.remove("d-none");
    }
});

//Checked if user is logged in
if (sessionStorage.getItem("loggedin") !== "1") {
    alert("U heeft niet de juiste rechten om deze pagina te zien, log eerst in.");
    window.location.href = "login.html"; // Redirecting to other page.
}

//Logout changes loggedin to "0"

function clearSession() {
    sessionStorage.clear();
    window.location.href = "login.html"; // Redirecting to other page.
}