var areaNameId = 3;
var subAreaNameId = 1;
var areaList = new Array();
var subAreaList1 = new Array();
var subAreaList2 = new Array();
var subAreaList3 = new Array();
var subAreaList4 = new Array();
var areaHasChild = new Array();
var areaHasNoChild = new Array();
var subAreaList = [];
var materialList = [];
var materialTypes = "";

//Send HTTP request
const Http = new XMLHttpRequest();
const url = 'http://localhost:8000/getMaterialTypes';
Http.open("GET", url);
Http.send();

//Wait for HTTP resonse
Http.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        materialTypes = Http.responseText;
    }
}

//Scroll to top button
$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
    } else {
        $('#back-to-top').fadeOut();
    }
});

//Scroll body to 0px on click
$('#back-to-top').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 0);
    return false;
});

function addFinalAreas(){
    var templateName = document.getElementById("inputTemplateName");
    var areas = document.getElementsByName("areas[]");
    var counter = 0;
    var check = 1;

    for (var i = 0; i < areas.length; i++){
        if (areas[i].value.length != 0){
            areaHasChild.push(areas[i].value.replace(/\s+/g,"_"));
            areaList[counter] = areas[i].value.replace(/\s+/g,"_");
            counter++;
        }
    }
    if(counter == 0) {
        document.getElementById("errorMessage").innerHTML = "Vul minimaal 1 gebied in.";
        document.getElementById("errorMessage").classList.remove("d-none")
        check = 0;
    }

    if(document.getElementById("inputTemplateName").value.length == 0) {
        document.getElementById("errorMessage").innerHTML = "Vul een template naam in.";
        document.getElementById("errorMessage").classList.remove("d-none")
        check = 0;
    }

    if (check == 1){
        $("#results").append("<input type='hidden' name='templateName' value='" + templateName.value + "'/>");
        document.getElementById("errorMessage").classList.add("d-none")
        $("#selectAreas").addClass("d-none");
            $("#add-extra-area").addClass("d-none");
            $("#add-area").addClass("d-none");
            $("#templateName").addClass("d-none");

            for (var j = 0; j < areaList.length; j++){
                $("#results").append("<input type='hidden' name='areasResult[]' value='" + areaList[j].replace("_", " ") + "'/>");
                $("#selectSubAreas").append(
                                    "<div class=\"form-group mt-5\" id=\"selectSubAreas" + areaList[j] + "\">" +
                                    "<label>Vul de subgebieden hieronder in voor " + areaList[j].replace("_", " ") + ":</label>" +
                                         "<div class=\"form-group input-group\" id=\"subAreaContainer" + areaList[j] + "1\">" +
                                             "<input id=\"subAreaName" + areaList[j] + "\" class=\"form-control\" type=\"text\" name=\"subAreas" + areaList[j] + "[]\" placeholder=\"Vul hier een gebied in\"/>" +
                                             "<div class=\"input-group-append\">" +
                                                 "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraSubArea(\'" + areaList[j] + "1\')\">Verwijderen</a>" +
                                             "</div>" +
                                         "</div>" +
                                         "<div class=\"form-group input-group\" id=\"subAreaContainer" + areaList[j] + "2\">" +
                                             "<input id=\"subAreaName" + areaList[j] + subAreaNameId + "\" class=\"form-control\" type=\"text\" name=\"subAreas" + areaList[j] + "[]\" placeholder=\"Vul hier een gebied in\"/>" +
                                             "<div class=\"input-group-append\">" +
                                                 "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraSubArea(\'" + areaList[j] + "2\')\">Verwijderen</a>" +
                                             "</div>" +
                                         "</div>" +
                                     "</div>" +
                                     "<a id=\"add-extra-area-" + areaList[j] + "\" type=\"button\" class=\"btn btn-primary add-area\" onclick=\"addExtraSubArea(\'" + areaList[j] + "\')\">Extra gebied toevoegen</a>" +
                                     "<a id=\"no-more-sub-areas-" + areaList[j] + "\" type=\"button\" class=\"btn btn-success add-area\" onclick=\"noMoreSubAreas(\'" + areaList[j] + "\')\">Dit gebied heeft geen subgebieden</a>");
            }
                            $("#selectSubAreas").append("<hr/><a id=\"add-sub-area\" type=\"button\" class=\"btn btn-success add-area\" onclick=\"addFinalSubAreas()\">Opslaan</a>");
            $("#selectSubAreas").removeClass("d-none");
            subAreaNameId++;
    }

}

function addFinalSubAreas(){

    $("#selectSubAreas").addClass("d-none");
    var areaCounter = 0;

    for (var i = 0; i < areaList.length; i++){
        var areas = document.getElementsByName("subAreas" + areaList[i] + "[]");

        for (var j = 0; j < areas.length; j++){
            if (areas[j].value.length != 0){
                subAreaList1.push(areas[j].value.replace(/\s+/g,"_"));
                areaHasChild.push(areas[j].value.replace(/\s+/g,"_"));
                $("#results").append("<input type='hidden' name='" + areaList[i].replace(/\s+/g,"_") + "Result[]' value='" + areas[j].value.replace(/\s+/g,"_") + "'/>");
                $("#selectSubAreas2").append(
                "<div class=\"form-group mt-5\" id=\"selectSubAreas" + areas[j].value.replace(/\s+/g,"_") + "\">" +
                "<label>Vul de subgebieden hieronder in voor " + areas[j].value.replace("_", " ") + ":</label>" +
                     "<div class=\"form-group input-group\" id=\"subAreaContainer" + areas[j].value + "1\">" +
                         "<input id=\"subAreaName" + areas[j].value.replace(/\s+/g,"_") + "\" class=\"form-control\" type=\"text\" name=\"subAreas" + areas[j].value.replace(/\s+/g,"_") + "[]\" placeholder=\"Vul hier een gebied in\"/>" +
                         "<div class=\"input-group-append\">" +
                             "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraSubArea(\'" + areas[j].value.replace(/\s+/g,"_") + "1\')\">Verwijderen</a>" +
                         "</div>" +
                     "</div>" +
                     "<div class=\"form-group input-group\" id=\"subAreaContainer" + areas[j].value.replace(/\s+/g,"_") + "2\">" +
                         "<input id=\"subAreaName" + areas[j].value.replace(/\s+/g,"_") + subAreaNameId + "\" class=\"form-control\" type=\"text\" name=\"subAreas" + areas[j].value.replace(/\s+/g,"_") + "[]\" placeholder=\"Vul hier een gebied in\"/>" +
                         "<div class=\"input-group-append\">" +
                             "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraSubArea(\'" + areas[j].value.replace(/\s+/g,"_") + "2\')\">Verwijderen</a>" +
                         "</div>" +
                     "</div>" +
                 "</div>" +
                 "<a id=\"add-extra-area-" + areas[j].value.replace(/\s+/g,"_") + "\" type=\"button\" class=\"btn btn-primary add-area\" onclick=\"addExtraSubArea(\'" + areas[j].value.replace(/\s+/g,"_") + "\')\">Extra gebied toevoegen</a>" +
                 "<a id=\"no-more-sub-areas-" + areas[j].value.replace(/\s+/g,"_") + "\" type=\"button\" class=\"btn btn-success add-area\" onclick=\"noMoreSubAreas(\'" + areas[j].value.replace(/\s+/g,"_") + "\')\">Dit gebied heeft geen subgebieden</a>");
                areaCounter++;
            }
            subAreaNameId++;
        }
    }
    if(areaCounter == 0){
        addFinalSubAreas4();
    }else{
        $("#selectSubAreas2").append("<hr/><a id=\"add-sub-area\" type=\"button\" class=\"btn btn-success add-area\" onclick=\"addFinalSubAreas2()\">Opslaan</a>");
        $("#selectSubAreas2").removeClass("d-none");
    }
}

function addFinalSubAreas2(){

    var areaCounter = 0;
    $("#selectSubAreas2").addClass("d-none");

    for (var i = 0; i < subAreaList1.length; i++){
        var areas = document.getElementsByName("subAreas" + subAreaList1[i] + "[]");
        for (var j = 0; j < areas.length; j++){
            if (areas[j].value.length != 0){
                subAreaList2.push(areas[j].value.replace(/\s+/g,"_"));
                areaHasChild.push(areas[j].value.replace(/\s+/g,"_"));
                $("#results").append("<input type='hidden' name='" + subAreaList1[i].replace(/\s+/g,"_") + "Result[]' value='" + areas[j].value.replace(/\s+/g,"_") + "'/>");
                $("#selectSubAreas3").append(
                    "<div class=\"form-group mt-5\" id=\"selectSubAreas" + areas[j].value.replace(/\s+/g,"_") + "\">" +
                    "<label>Vul de subgebieden hieronder in voor " + areas[j].value.replace("_", " ") + ":</label>" +
                         "<div class=\"form-group input-group\" id=\"subAreaContainer" + areas[j].value + "1\">" +
                             "<input id=\"subAreaName" + areas[j].value.replace(/\s+/g,"_") + "\" class=\"form-control\" type=\"text\" name=\"subAreas" + areas[j].value.replace(/\s+/g,"_") + "[]\" placeholder=\"Vul hier een gebied in\"/>" +
                             "<div class=\"input-group-append\">" +
                                 "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraSubArea(\'" + areas[j].value.replace(/\s+/g,"_") + "1\')\">Verwijderen</a>" +
                             "</div>" +
                         "</div>" +
                         "<div class=\"form-group input-group\" id=\"subAreaContainer" + areas[j].value.replace(/\s+/g,"_") + "2\">" +
                             "<input id=\"subAreaName" + areas[j].value.replace(/\s+/g,"_") + subAreaNameId + "\" class=\"form-control\" type=\"text\" name=\"subAreas" + areas[j].value.replace(/\s+/g,"_") + "[]\" placeholder=\"Vul hier een gebied in\"/>" +
                             "<div class=\"input-group-append\">" +
                                 "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraSubArea(\'" + areas[j].value.replace(/\s+/g,"_") + "2\')\">Verwijderen</a>" +
                             "</div>" +
                         "</div>" +
                     "</div>" +
                     "<a id=\"add-extra-area-" + areas[j].value.replace(/\s+/g,"_") + "\" type=\"button\" class=\"btn btn-primary add-area\" onclick=\"addExtraSubArea(\'" + areas[j].value.replace(/\s+/g,"_") + "\')\">Extra gebied toevoegen</a>" +
                     "<a id=\"no-more-sub-areas-" + areas[j].value.replace(/\s+/g,"_") + "\" type=\"button\" class=\"btn btn-success add-area\" onclick=\"noMoreSubAreas(\'" + areas[j].value.replace(/\s+/g,"_") + "\')\">Dit gebied heeft geen subgebieden</a>");
                    areaCounter++;
            }
            subAreaNameId++;
        }
    }
    if(areaCounter == 0){
        addFinalSubAreas4();
    }else{
        $("#selectSubAreas3").append("<hr/><a id=\"add-sub-area\" type=\"button\" class=\"btn btn-success add-area\" onclick=\"addFinalSubAreas3()\">Opslaan</a>");
        $("#selectSubAreas3").removeClass("d-none");
    }
}

function addFinalSubAreas3(){

    var areaCounter = 0;
    $("#selectSubAreas3").addClass("d-none");

    for (var i = 0; i < subAreaList2.length; i++){
        var areas = document.getElementsByName("subAreas" + subAreaList2[i] + "[]");

        for (var j = 0; j < areas.length; j++){
            if (areas[j].value.length != 0){
                subAreaList3.push(areas[j].value.replace(/\s+/g,"_"));
                areaHasChild.push(areas[j].value.replace(/\s+/g,"_"));
                $("#results").append("<input type='hidden' name='" + subAreaList2[i].replace(/\s+/g,"_") + "Result[]' value='" + areas[j].value.replace(/\s+/g,"_") + "'/>");
                $("#selectSubAreas4").append(
                "<div class=\"form-group mt-5\" id=\"selectSubAreas" + areas[j].value.replace(/\s+/g,"_") + "\">" +
                "<label>Vul de subgebieden hieronder in voor " + areas[j].value.replace("_", " ") + ":</label>" +
                     "<div class=\"form-group input-group\" id=\"subAreaContainer" + areas[j].value + "1\">" +
                         "<input id=\"subAreaName" + areas[j].value.replace(/\s+/g,"_") + "\" class=\"form-control\" type=\"text\" name=\"subAreas" + areas[j].value.replace(/\s+/g,"_") + "[]\" placeholder=\"Vul hier een gebied in\"/>" +
                         "<div class=\"input-group-append\">" +
                             "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraSubArea(\'" + areas[j].value.replace(/\s+/g,"_") + "1\')\">Verwijderen</a>" +
                         "</div>" +
                     "</div>" +
                     "<div class=\"form-group input-group\" id=\"subAreaContainer" + areas[j].value.replace(/\s+/g,"_") + "2\">" +
                         "<input id=\"subAreaName" + areas[j].value.replace(/\s+/g,"_") + subAreaNameId + "\" class=\"form-control\" type=\"text\" name=\"subAreas" + areas[j].value.replace(/\s+/g,"_") + "[]\" placeholder=\"Vul hier een gebied in\"/>" +
                         "<div class=\"input-group-append\">" +
                             "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraSubArea(\'" + areas[j].value.replace(/\s+/g,"_") + "2\')\">Verwijderen</a>" +
                         "</div>" +
                     "</div>" +
                 "</div>" +
                 "<a id=\"add-extra-area-" + areas[j].value.replace(/\s+/g,"_") + "\" type=\"button\" class=\"btn btn-primary add-area\" onclick=\"addExtraSubArea(\'" + areas[j].value.replace(/\s+/g,"_") + "\')\">Extra gebied toevoegen</a>" +
                 "<a id=\"no-more-sub-areas-" + areas[j].value.replace(/\s+/g,"_") + "\" type=\"button\" class=\"btn btn-success add-area\" onclick=\"noMoreSubAreas(\'" + areas[j].value.replace(/\s+/g,"_") + "\')\">Dit gebied heeft geen subgebieden</a>");
                areaCounter++;
            }
            subAreaNameId++;
        }
    }
    if(areaCounter == 0){
        addFinalSubAreas4();
    }else{
        $("#selectSubAreas4").append("<hr/><a id=\"add-sub-area\" type=\"button\" class=\"btn btn-success add-area\" onclick=\"addFinalSubAreas4()\">Opslaan</a>");
        $("#selectSubAreas4").removeClass("d-none");
    }
}

function addFinalSubAreas4(){

    var areaCounter = 0;
    $("#selectSubAreas4").addClass("d-none");

    for (var i = 0; i < subAreaList3.length; i++){
        var areas = document.getElementsByName("subAreas" + subAreaList3[i] + "[]");
        for (var j = 0; j < areas.length; j++){
            if (areas[j].value.length != 0){
                subAreaList4.push(areas[j].value.replace(/\s+/g,"_"));
                areaHasNoChild.push(areas[j].value.replace(/\s+/g,"_"));
                $("#results").append("<input type='hidden' name='" + subAreaList3[i].replace(/\s+/g,"_") + "Result[]' value='" + areas[j].value.replace(/\s+/g,"_") + "'/>");
            }
            subAreaNameId++;
        }
    }

    for (var j = 0; j < areaHasNoChild.length; j++){
        $("#selectMaterials").append("<div id='" + areaHasNoChild[j] + "Materials'>Kies de materialen voor " + areaHasNoChild[j] + ": " +
        "<div class=\"form-group\" id=\"templateName\">" +
        "<select onchange='addExtraMaterial(\"" + areaHasNoChild[j] + "\")' name='" + areaHasNoChild[j] + "Material[]' class='form-control text-capitalize mt-1'><option value='' selected disabled hidden>Kies een materiaalsoort..</option>" + materialTypes + "</select>" +
        "</div></div>");
    }
    $("#selectMaterials").removeClass("d-none");
    $("#selectMaterials").append("<a id=\"add-template\" type=\"button\" class=\"btn btn-success add-area float-right\" onclick=\"saveFinalTemplate()\">Template opslaan</a>");
}

function addExtraMaterial(area){
    $("#" + area + "Materials").append("<div class=\"form-group\" id=\"templateName\"><select onchange='addExtraMaterial(\"" + area + "\")' name='" + area + "Material[]' class='form-control text-capitalize mt-1'><option value='' selected disabled hidden>Kies een materiaalsoort..</option>" + materialTypes + "</select></div>");
}

function addExtraArea(){
    $("#selectAreas").append("<div class=\"form-group input-group\" id=\"areaContainer" + areaNameId + "\">"+
          "<input id=\"areaName" + areaNameId + "\" class=\"form-control\" type=\"text\" name=\"areas[]\" placeholder=\"Vul hier een gebied in\"/>"+
          "<div class=\"input-group-append\">"+
              "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraArea(" + areaNameId + ")\">Verwijderen</a>"+
          "</div>"+
     "</div>");
    areaNameId++;
}

function addExtraSubArea(area){
    $("#selectSubAreas" + area).append("<div class=\"form-group input-group\" id=\"subAreaContainer" + area + subAreaNameId + "\">"+
          "<input id=\"areaName" + area + subAreaNameId + "\" class=\"form-control\" type=\"text\" name=\"subAreas" + area + "[]\" placeholder=\"Vul hier een gebied in\"/>"+
          "<div class=\"input-group-append\">"+
              "<a type=\"button\" class=\"btn btn-danger\" onclick=\"deleteExtraSubArea(\'" + area + subAreaNameId + "\')\">Verwijderen</a>"+
          "</div>"+
     "</div>");
    subAreaNameId++;
}

function deleteExtraArea(elementId){
    $("#areaContainer" + elementId).remove();
}

function deleteExtraSubArea(elementId){
    $("#subAreaContainer" + elementId).remove();
}

function noMoreSubAreas(area){
    areaHasNoChild.push(area);
    var index = areaHasChild.indexOf(area);
    if (index > -1) {
      areaHasChild.splice(index, 1);
    }
    $("#selectSubAreas" + area).addClass("d-none");
    $("#add-extra-area-" + area).addClass("d-none");
    $("#no-more-sub-areas-" + area).addClass("d-none");
}

function saveFinalTemplate(){
    var materialList = [];
    var subAreaList = [];

    for (var i = 0; i < areaHasChild.length; i++){
        var areas = document.getElementsByName(areaHasChild[i] + "Result[]");
        var array = []

        for (var j = 0; j < areas.length; j++){
            if (areas[j].value.length != 0){
                array.push(areas[j].value);
            }
        }
        var areaName = areaHasChild[i];
        subAreaList.push({[areaName] : array});
    }

    for (var k = 0; k < areaHasNoChild.length; k++){
        var materials = document.getElementsByName(areaHasNoChild[k] + "Material[]");
        var array2 = [];

        for (var l = 0; l < materials.length; l++){

            if (materials[l].selectedIndex != ""){
                array2.push(materials[l].options[materials[l].selectedIndex].value);
            }
        }
        var areaName2 = areaHasNoChild[k];
        materialList.push({[areaName2] : array2});
    }

    var templateName = document.getElementsByName("templateName")[0].value.replace("_", " ")

    var dict = {
            "Template" : templateName,
            "Areas with child" : areaHasChild,
            "Areas without child" : areaHasNoChild,
            "Areas" : areaList,
            "Area amount" : areaList.length,
            "Subareas" : subAreaList,
            "Materials" : materialList
            };

    var dictstring = JSON.stringify(dict);

    $.post("http://localhost:8000/addtemplate", dictstring, function(status){
        if(status == "success"){
            $("#successMessage").html("De template is toegevoegd. <a href='./templates.html'>Terug naar templates</a>");
            $("#successMessage").removeClass("d-none");
        }else{
            $("#errorMessage").html("Er is iets mis gegaan. Probeer het later opnieuw");
            $("#errorMessage").removeClass("d-none");
        }
    });
}