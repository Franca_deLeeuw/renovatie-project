//Checked if user is admin, else send them back to login
if (sessionStorage.getItem("admin") !== "1") {
    sessionStorage.clear();
    alert("U heeft niet de juiste rechten om deze pagina te zien, log eerst in.");
    window.location.href = "login.html"; // Redirecting to other page.
}

//Send HTTP request
const Http2 = new XMLHttpRequest();
const url2 = 'http://localhost:8000/getmaterialdata';
Http2.open("GET", url2);
Http2.send();

//Wait for HTTP resonse
Http2.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var html = Http2.responseText;

        var split = html.split("~");

        var materialtype = split[0];
        var materialorigin = split[1];
        var materialorigintype = split[2];

        $("#select_material").append(materialtype);
        $("#material_type_origin").append(materialorigin);
        $("#material_type_origin_distance").append(materialorigintype);

    }
}

// Below function Executes on click of login button.
function register() {
    //retrieve data from user input fields
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var email = document.getElementById("email").value;
    var firstname = document.getElementById("firstname").value;
    var lastname = document.getElementById("lastname").value;
    var isadmin = document.getElementById("isadmin").value;

//Send HTTP request
    //set register data into http request url (should be json, but we had no time)
    const Http = new XMLHttpRequest();
    const url = 'http://localhost:8000/register?username=' + username + '&password=' + password + '&email=' + email + '&firstname=' + firstname + '&lastname=' + lastname + '&isadmin=' + isadmin;
    Http.open("GET", url);
    Http.send();


//Wait for HTTP resonse
    Http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //on successful registration shouw success notification and empty out input fields
            if (Http.responseText > 0) {
                document.getElementById("registration-success").innerHTML = "Nieuwe gebruiker aangemaakt.";
                document.getElementById("registration-success").classList.remove("registrationsucces-hidden");

                document.getElementById("username").value = "";
                document.getElementById("password").value = "";
                document.getElementById("email").value = "";
                document.getElementById("firstname").value = "";
                document.getElementById("lastname").value = "";
                document.getElementById("isadmin").value = "0";

            } else {
                document.getElementById("registration-error").innerHTML = "Er is iets misgegaan..";
                document.getElementById("registration-error").classList.remove("registrationerror-hidden");
            }

        }
    }

}

function insertMaterial() {
    //retrieve input from fields
    var select_material = document.getElementById("select_material").value;
    var material_type_name = document.getElementById("material_type_name").value;
    var material_type_origin = document.getElementById("material_type_origin").value;
    var material_type_origin_distance = document.getElementById("material_type_origin_distance").value;
    var material_type_unit = document.getElementById("material_type_unit").value;
    var material_type_embodied_energy = document.getElementById("material_type_embodied_energy").value;
    var material_type_embodied_co2 = document.getElementById("material_type_embodied_co2").value;
    var material_type_lifespan = document.getElementById("material_type_lifespan").value;
    var material_type_releasability = document.getElementById("material_type_releasability").value;
    var material_type_path_to_image = document.getElementById("material_type_path_to_image").value;

    //if no name has been filled in, use origin selection
    if (material_type_name == null || material_type_name == ""){
        material_type_name = document.getElementById("material_type_origin").options[document.getElementById("material_type_origin").value].text;
    }

    //push user input into array and turn it into json
    var materialArray = [];
    materialArray.push(material_type_name, material_type_origin, material_type_origin_distance,
        material_type_unit, material_type_embodied_energy, material_type_embodied_co2, material_type_lifespan, material_type_releasability, material_type_path_to_image, select_material);

    var json = {"materialInfo": materialArray};


    var jsonString = JSON.stringify(json);

    //use http request to send data to backend with json
    const Http = new XMLHttpRequest();
    const url = 'http://localhost:8000/insertmaterial';
    Http.open("POST", url);
    Http.send(jsonString);

    Http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //show succes notification on successful insertion
            if (Http.responseText > 0) {
                document.getElementById("material-insert-success").innerHTML = "Materiaal aangemaakt.";
                document.getElementById("material-insert-success").classList.remove("material-insert-success-hidden");

                document.getElementById("select_material").value = "";
                document.getElementById("material_type_name").value = "";
                document.getElementById("material_type_origin").value = "";
                document.getElementById("material_type_origin_distance").value = "";
                document.getElementById("material_type_unit").value = "";
                document.getElementById("material_type_embodied_energy").value = "";
                document.getElementById("material_type_embodied_co2").value = "";
                document.getElementById("material_type_lifespan").value = "";
                document.getElementById("material_type_releasability").value = "";
                document.getElementById("material_type_path_to_image").value = "";
            } else {
                document.getElementById("registration-error").innerHTML = "Er is iets misgegaan..";
                document.getElementById("registration-error").classList.remove("registrationerror-hidden");
            }

        }
    }
}

function insertMaterialType() {

    var material_type_name = document.getElementById("material_type_name_insert").value.replace(/\s+/g,"_");

    //Send HTTP request
        const Http = new XMLHttpRequest();
        const url = 'http://localhost:8000/insertMaterialType?materialType=' + material_type_name;
        Http.open("GET", url);
        Http.send();


    //Wait for HTTP resonse
        Http.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                if (Http.responseText > 0) {
                    document.getElementById("material-type-insert-success").innerHTML = "Nieuwe materiaalsoort aangemaakt.";
                    document.getElementById("material-type-insert-success").classList.remove("material-insert-success-hidden");

                    document.getElementById("material_type_name_insert").value = "";

                } else {
                    document.getElementById("material-type-insert-error").innerHTML = "Er is iets misgegaan..";
                    document.getElementById("material-type-insert-error").classList.remove("material-insert-success-hidden");
                }

            }
        }
}