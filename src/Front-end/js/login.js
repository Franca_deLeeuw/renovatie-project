
// Below function Executes on click of login button.
function validate() {
//retrieve values from input fields and sent them to the backend with a http request
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

//Send HTTP request
//data is send through with a url
    const Http = new XMLHttpRequest();
    const url='http://localhost:8000/login?username=' + username + '&password=' + password;
    Http.open("GET", url);
    Http.send();


//Wait for HTTP resonse
    Http.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            //split the http response
            userData = Http.responseText.split(",");

            //if the user logged in successfully set the data in sessionstorage for later use, if not show an error message
            if (userData[0] == "0") {
                document.getElementById("login-error").innerHTML = "De combinatie van gebruikersnaam en wachtwoord is onjuist, probeer het opnieuw";
                document.getElementById("login-error").classList.remove("login-hidden");
            } else {
                sessionStorage.setItem("userid", userData[0]);
                sessionStorage.setItem("admin", userData[1]);
                sessionStorage.setItem("loggedin", "1");
                window.location.href = "templates.html";
            }
        }
    }

}
