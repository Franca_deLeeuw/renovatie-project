//Send HTTP request
const Http = new XMLHttpRequest();
var templateId = sessionStorage.getItem("templateId");
var userId = sessionStorage.getItem("userid");
var progressbarMax = 0;
var progressBarCurrent = 0;

const url='http://localhost:8000/configuratormenu?templateId=' + templateId;
Http.open("GET", url);
Http.send();

//Wait for HTTP response
Http.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       document.getElementById("main_menu").innerHTML = Http.responseText;
    }
};

const Http2 = new XMLHttpRequest();
const url2='http://localhost:8000/configurator?templateId=' + templateId + '&userId=' + userId;
Http2.open("GET", url2);
Http2.send();


//Wait for HTTP response
Http2.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       document.getElementById("content").innerHTML = Http2.responseText;

    }
};

const Http3 = new XMLHttpRequest();
const url3 ='http://localhost:8000/progressbar?templateId=' + templateId;
Http3.open("GET", url3);
Http3.send();

//Wait for HTTP response
Http3.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        // Typical action to be performed when the document is ready:
        var response = Http3.responseText.split("~");
        document.getElementById("totals").innerHTML = response[0];
        document.getElementById("embodiedEnergy").innerHTML = response[1];
        progressbarMax = Number(response[1]);
        progressBarCurrent = Number(response[1]);
    }
};

//Show image when option is selected
function showFormatImage(imgId, itemId, imgFrameId) {
    if ($('select[name=' + itemId + '] option:selected').val() != 'default'){
        var image = document.getElementById(imgId);
        image.src = "images/" + $('select[name=' + itemId + '] option:selected').attr('data-img');
        var imageFrame = document.getElementById(imgFrameId);
        imageFrame.classList.remove("hidden");
    }else{
        var imageFrame = document.getElementById(imgFrameId);
        imageFrame.classList.add("hidden");
    }

    var id = $('select[name=' + itemId + '] option:selected').val();

    const Http3 = new XMLHttpRequest();
    const url3='http://localhost:8000/getMaterial?materialId=' + id;
    Http3.open("GET", url3);
    Http3.send();


    //Wait for HTTP response
    Http3.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
           // Typical action to be performed when the document is ready:
           var oldEmbodiedEnergy = Number(document.getElementById(imgFrameId).getElementsByTagName('label')[3].innerHTML);

           var split = Http3.responseText.split("_");

           document.getElementById(imgFrameId).getElementsByTagName('label')[0].innerHTML = split[0];
           document.getElementById(imgFrameId).getElementsByTagName('label')[1].innerHTML = split[1];
           document.getElementById(imgFrameId).getElementsByTagName('label')[2].innerHTML = split[2];
           document.getElementById(imgFrameId).getElementsByTagName('label')[3].innerHTML = split[3];
           document.getElementById(imgFrameId).getElementsByTagName('label')[4].innerHTML = split[4];
           document.getElementById(imgFrameId).getElementsByTagName('label')[5].innerHTML = split[5];
           document.getElementById(imgFrameId).getElementsByTagName('label')[6].innerHTML = split[6];
           document.getElementById(imgFrameId).getElementsByTagName('label')[7].innerHTML = split[7];


           progressBarCurrent = progressBarCurrent - oldEmbodiedEnergy + Number(split[3]);
           var updatedValue = (progressBarCurrent / progressbarMax) * 100;

           if(updatedValue < 100) {
               $('.progress-bar').css('background-color', "#dc3545");
           }
           if(updatedValue < 75) {
               $('.progress-bar').css('background-color', "#ff9707");
           }
           if(updatedValue < 60) {
               $('.progress-bar').css('background-color', "#ffd007");
           }
           if(updatedValue < 45) {
               $('.progress-bar').css('background-color', "#28a745");
           }

           $('.progress-bar').css('width', updatedValue+'%');

           $('#embodiedEnergy').html(progressBarCurrent);

        }
    };

   return false;
}

//Scroll to top button
$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
    } else {
        $('#back-to-top').fadeOut();
    }
});


//Scroll body to 0px on click
$('#back-to-top').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 0);
    return false;
});

//Menu toggle scripts
$(document).ready(function() {
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#page-content-wrapper").toggleClass("toggled");
    });
    $("#menu-toggle-mobile").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#page-content-wrapper").toggleClass("toggled");
    });

});
