//Send HTTP request
const Http = new XMLHttpRequest();
const url='http://localhost:8000/templates';
Http.open("GET", url);
Http.send();

//Wait for HTTP resonse
Http.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       document.getElementById("content").innerHTML = Http.responseText;
    }
};

function setTemplate(templateId) {
    sessionStorage.setItem("templateId", templateId);
    window.location.href = "index.html"; // Redirecting to other page.
}

//Scroll to top button
$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
    } else {
        $('#back-to-top').fadeOut();
    }
});


//Scroll body to 0px on click
$('#back-to-top').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 0);
    return false;
});