package Main.MaterialType;

public class MaterialType {

    private int materialTypeId;
    private int materialTypeSubAreaId;
    private String materialTypeName;

    public MaterialType(){

    }

    public MaterialType(int materialTypeId, int materialTypeSubAreaId, String materialTypeName) {
        this.materialTypeId = materialTypeId;
        this.materialTypeSubAreaId = materialTypeSubAreaId;
        this.materialTypeName = materialTypeName;
    }

    public int getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(int materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    public int getMaterialTypeSubAreaId() {
        return materialTypeSubAreaId;
    }

    public void setMaterialTypeSubAreaId(int materialTypeSubAreaId) {
        this.materialTypeSubAreaId = materialTypeSubAreaId;
    }

    public String getMaterialTypeName() {
        return materialTypeName;
    }

    public void setMaterialTypeName(String materialTypeName) {
        this.materialTypeName = materialTypeName;
    }
}
