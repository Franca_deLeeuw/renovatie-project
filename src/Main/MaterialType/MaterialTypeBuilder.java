package Main.MaterialType;

import Main.DatabaseHandler;
import Main.Material.Material;

import java.util.ArrayList;

public class MaterialTypeBuilder {

    public ArrayList<MaterialType> BuildMaterialTypeListFromSubAreaId(int subAreaId){
        DatabaseHandler databaseHandler = new DatabaseHandler();
        return databaseHandler.getMaterialTypesFromSubAreaId(subAreaId);
    }

    public String buildMaterialTypeList(){
        ArrayList<MaterialType> materialTypes = new ArrayList<>();
        DatabaseHandler databaseHandler = new DatabaseHandler();
        materialTypes = databaseHandler.getAllMaterialTypes();
        String html = "";

        for (int i = 0; i < materialTypes.size(); i++) {
            html += "<option class='text-capitalize' value='" + materialTypes.get(i).getMaterialTypeId() + "'>" + materialTypes.get(i).getMaterialTypeName() + "</option>";
        }

        return html;
    }

    public int insertMaterialType(String materialType){
        DatabaseHandler databaseHandler = new DatabaseHandler();
        return databaseHandler.insertMaterialType(materialType);
    }

}
