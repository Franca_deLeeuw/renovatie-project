package Main;

import Main.Admin.AdminBuilder;
import Main.Area.AreaBuilder;
import Main.Configuration.ConfigurationBuilder;
import Main.Material.Material;
import Main.Material.MaterialBuilder;
import Main.MaterialType.MaterialTypeBuilder;
import com.mysql.cj.xdevapi.JsonNumber;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import Main.Templates.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.security.MessageDigest;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


import javax.xml.crypto.Data;

public class Main {
    private static String[] signInData;
    private static String[] registerData;
    private static String[] insertMaterialData;

    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/templates", new TemplatesHandler());
        server.createContext("/addtemplate", new AddTemplateHandler());
        server.createContext("/configuratormenu", new ConfiguratorMenuHandler());
        server.createContext("/configurator", new ConfiguratorHandler());
        server.createContext("/login", new LoginHandler());
        server.createContext("/register", new RegisterHandler());
        server.createContext("/insertmaterial", new InsertMaterialHandler());
        server.createContext("/getMaterial", new MaterialHandler());
        server.createContext("/getmaterialdata", new GetMaterialDataHandler());
        server.createContext("/insertMaterialType", new InsertMaterialTypeHandler());
        server.createContext("/progressbar", new progressbarHandler());
        server.createContext("/getMaterialTypes", new GetmaterialTypeHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
        System.out.println("Server is running...");
    }

    static class MaterialHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/getMaterial");

            URI requestURI = t.getRequestURI();
            String templateData = "" + requestURI;
            String[] removeUrl = templateData.split("\\?");
            String[] templateSplit = removeUrl[1].split("=");

            MaterialBuilder materialBuilder = new MaterialBuilder();
            Material material = materialBuilder.getMaterialsFromMaterialId(Integer.parseInt(templateSplit[1]));

            String response = material.getMaterialOrigin() + "_" + material.getMaterialOriginDistance() + "_" + material.getMaterialUnit() + "_" + material.getMaterialEmbodiedEnergy() + "_" + material.getMaterialEmbodiedCo2() + "_" + material.getMaterialLifeSpan() + "_" + material.getMaterialReleasability() + "_" + material.getMaterialStock();

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class TemplatesHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/templates");
            TemplatesBuilder templatesbuilder = new TemplatesBuilder();
            String response = templatesbuilder.buildAllTemplates();
            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class AddTemplateHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/addtemplates");

            InputStreamReader isr = new InputStreamReader(t.getRequestBody(), "utf-8");
            TemplatesBuilder templatesBuilder = new TemplatesBuilder();
            String response = templatesBuilder.insertTemplate(isr);

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class ConfiguratorHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/configurator");

            URI requestURI = t.getRequestURI();
            String templateData = "" + requestURI;
            String[] removeUrl = templateData.split("\\?");
            String[] templateSplit = removeUrl[1].split("=");
            String[] templateId = templateSplit[1].split("&");

            TemplatesBuilder templatesBuilder = new TemplatesBuilder();
            templatesBuilder.createDefaultTemplate(Integer.parseInt(templateId[0]), Integer.parseInt(templateSplit[2]));

            AreaBuilder areaBuilder = new AreaBuilder();
            String response = areaBuilder.buildAllAreas(templateId[0]);

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class ConfiguratorMenuHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/configuratormenu");
            URI requestURI = t.getRequestURI();
            String templateData = "" + requestURI;

            String[] removeUrl = templateData.split("\\?");
            String[] templateSplit = removeUrl[1].split("=");
            AreaBuilder areaBuilder = new AreaBuilder();
            String response = areaBuilder.buildMenu(templateSplit[1]);
            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class progressbarHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/progressbar");
            URI requestURI = t.getRequestURI();
            String templateData = "" + requestURI;
            String[] removeUrl = templateData.split("\\?");
            String[] templateSplit = removeUrl[1].split("=");

            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            String response = configurationBuilder.getSavedConfigurations(Integer.parseInt(templateSplit[1]));

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }


    static class LoginHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/login");
            //get the url with the login data
            URI requestURI = t.getRequestURI();

            String loginUrl = "" + requestURI;
            //strip unnecessary characters from string
            try {
                loginRemoveCharactersAndHashPassword(loginUrl);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            DatabaseHandler databaseHandler = new DatabaseHandler();
            //call the database and login the user
            try {
                String[] userData = databaseHandler.signInUser(signInData[0], signInData[1]);
                if (userData[0] != null) {
                    //if the user[0] is not null there is a user and the needed data is filled into the response
                    loginUrl = "" + userData[0] + "," + userData[1];
                } else {
                    loginUrl = "0,0";
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, loginUrl.length());
            OutputStream os = t.getResponseBody();
            os.write(loginUrl.getBytes());
            os.close();

        }
    }

    static class RegisterHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/register");
            URI requestURI = t.getRequestURI();
            String response = "";

            String registerUrl = "" + requestURI;
            //split the url into useful parts(this works, but it should be json)
            try {
                registerRemoveCharactersAndHashPassword(registerUrl);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            DatabaseHandler databaseHandler = new DatabaseHandler();
            //response is send back to admin.js and used there
            try {
                response = String.valueOf(databaseHandler.registerUser(registerData[0], registerData[2], registerData[3], registerData[4], registerData[1], Integer.parseInt(registerData[5])));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class InsertMaterialHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            String response = "";
            System.out.println("/insertmaterial");
            //retrieve json and turn it into a usable array
            InputStreamReader isr = new InputStreamReader(t.getRequestBody(), "utf-8");
            BufferedReader br = new BufferedReader(isr);
            int b;
            StringBuilder buf = new StringBuilder(512);
            while ((b = br.read()) != -1) {
                buf.append((char) b);
            }
            br.close();
            isr.close();
            JSONParser parser = new JSONParser();
            String[] materialInfo = new String[10];
            int counter = 0;
            try {
                Object obj = parser.parse(buf.toString());
                JSONObject jsonObject = (JSONObject) obj;
                JSONArray materialInfoJson = (JSONArray) jsonObject.get("materialInfo");
                Iterator<JSONObject> iterator = materialInfoJson.iterator();
                while (iterator.hasNext()) {
                    materialInfo[counter] = String.valueOf(iterator.next());
                    counter++;
                }
                //send the array to the databasehandler and insert the material
                DatabaseHandler databaseHandler = new DatabaseHandler();
                response = String.valueOf(databaseHandler.insertMaterial(materialInfo[0], Integer.parseInt(materialInfo[1]), Integer.parseInt(materialInfo[2]), Double.parseDouble(materialInfo[3]),
                        Double.parseDouble(materialInfo[4]), materialInfo[5], Integer.parseInt(materialInfo[6]), Double.parseDouble(materialInfo[7]), materialInfo[8], Integer.parseInt(materialInfo[9])));

            } catch (Exception e) {
                e.printStackTrace();
            }

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();


        }
    }

    static void loginRemoveCharactersAndHashPassword(String loginUrl) throws NoSuchAlgorithmException {
        //remove unnecessary characters from the URL
        String[] removeUrl = loginUrl.split("\\?");
        String[] loginSplit = removeUrl[1].split("&");
        String[] username = loginSplit[0].split("=");
        String[] password = loginSplit[1].split("=");

        //hash password
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        byte[] data = md.digest(password[1].getBytes());

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < data.length; i++) {
            sb.append(String.format("%02x", data[i]));
        }
        String hashpass = sb.toString();

        password[1] = hashpass;

        //only keep the username and hashed password
        signInData = new String[]{username[1], password[1]};

    }

    static void registerRemoveCharactersAndHashPassword(String RegisterUrl) throws NoSuchAlgorithmException {
        //remove unnecessary characters from the URL
        String[] removeUrl = RegisterUrl.split("\\?");
        String[] registerSplit = removeUrl[1].split("&");
        String[] username = registerSplit[0].split("=");
        String[] password = registerSplit[1].split("=");
        String[] email = registerSplit[2].split("=");
        String[] firstname = registerSplit[3].split("=");
        String[] lastname = registerSplit[4].split("=");
        String[] isadmin = registerSplit[5].split("=");

        //hash password
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        byte[] data = md.digest(password[1].getBytes());

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < data.length; i++) {
            sb.append(String.format("%02x", data[i]));
        }
        String hashpass = sb.toString();

        password[1] = hashpass;

        registerData = new String[]{username[1], password[1], email[1], firstname[1], lastname[1], isadmin[1]};

    }

    static class GetMaterialDataHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/getmaterialdata");

            AdminBuilder adminBuilder = new AdminBuilder();

            String response = adminBuilder.getMaterialData();

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class InsertMaterialTypeHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/insertMaterialType");
            URI requestURI = t.getRequestURI();
            String templateData = "" + requestURI;

            String[] removeUrl = templateData.split("\\?");
            String[] templateSplit = removeUrl[1].split("=");

            MaterialTypeBuilder materialTypeBuilder = new MaterialTypeBuilder();
            int sqlResponse = materialTypeBuilder.insertMaterialType(templateSplit[1].replace("_", " "));

            String response = String.valueOf(sqlResponse);

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class GetmaterialTypeHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            System.out.println("/getMaterialTypes");

            MaterialTypeBuilder materialTypeBuilder = new MaterialTypeBuilder();
            String response = materialTypeBuilder.buildMaterialTypeList();

            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            t.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");

            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
}




