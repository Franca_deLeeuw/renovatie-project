package Main;

import Main.Area.Area;
import Main.Configuration.Configuration;
import Main.Material.Material;
import Main.MaterialType.MaterialType;
import Main.Templates.*;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

public class DatabaseHandler {

    Connection conn = null;

    public DatabaseHandler() {
        try {
            String dbName = "renovation";
            String dbUserName = "RaspberryDatabase";
            String dbPassword = "3U5pKs2xy3xVXCGv";
            conn = DriverManager.getConnection("jdbc:mysql://77.175.159.177:3306/" + dbName + "?user=" + dbUserName + "&password=" + dbPassword + "&useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC");
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    public ArrayList<Templates> getAllTemplates() {
        ResultSet rs;
        ArrayList<Templates> templates = new ArrayList<>();

        String query = "SELECT name, id FROM templates";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);

            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String templateName = rs.getString("name");
                int templateId = rs.getInt("id");
                Templates template = new Templates(templateName, templateId);
                templates.add(template);
            }
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return templates;
    }

    public String[][] getAreasFromTemplate(int templateId) {
        ResultSet rs;
        String query = "SELECT COUNT(name) FROM areas WHERE template_id = " + templateId;
        int areaAmount = 0;
        try {

            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                areaAmount = rs.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        ResultSet rs2;
        String query2 = "SELECT name, id FROM areas WHERE template_id = " + templateId;
        String[][] areas = new String[areaAmount][2];
        try {

            PreparedStatement preparedStatement = conn.prepareStatement(query2);
            rs2 = preparedStatement.executeQuery();

            int counter = 0;
            while (rs2.next()) {
                String areaName = rs2.getString("name");
                int areaId = rs2.getInt("id");
                areas[counter][0] = areaName;
                areas[counter][1] = String.valueOf(areaId);
                counter++;
            }
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return areas;
    }

    public String[] getSubAreasFromTemplate(int subAreaId) {
        ResultSet rs;
        String query = "SELECT COUNT(name) FROM sub_areas WHERE area_id = " + subAreaId;
        int subAreaAmount = 0;
        try {

            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                subAreaAmount = rs.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        String subAreas[] = new String[subAreaAmount];

        ResultSet rs2;
        String query2 = "SELECT name FROM sub_areas WHERE area_id = " + subAreaId;
        try {

            PreparedStatement preparedStatement = conn.prepareStatement(query2);
            rs2 = preparedStatement.executeQuery();

            int counter = 0;
            while (rs2.next()) {
                String subAreaName = rs2.getString("name");
                subAreas[counter] = subAreaName;
                counter++;
            }
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return subAreas;
    }

    public ArrayList<Area> getAllAreas(int templateId) {
        ArrayList<Area> areas = new ArrayList<>();
        ResultSet rs;
        String query = "SELECT name, id FROM areas WHERE template_id = " + templateId;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String areaName = rs.getString("name");
                int areaId = rs.getInt("id");

                Area area = new Area(areaName, areaId);
                areas.add(area);
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return areas;
    }

    public String[][] getSubAreasFromArea(int AreaId, int sub) {
        ResultSet rs;
        String query;
        if (sub == 1) {
            query = "SELECT COUNT(name) FROM sub_areas WHERE sub_area_id = " + AreaId;
        } else {
            query = "SELECT COUNT(name) FROM sub_areas WHERE area_id = " + AreaId;
        }

        int subAreaAmount = 0;
        try {

            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                subAreaAmount = rs.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        String subAreas[][] = new String[subAreaAmount][2];

        String query2;
        if (sub == 1) {
            query2 = "SELECT name, id FROM sub_areas WHERE sub_area_id = " + AreaId;
        } else {
            query2 = "SELECT name, id FROM sub_areas WHERE area_id = " + AreaId;
        }
        ResultSet rs2;


        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query2);
            rs2 = preparedStatement.executeQuery();

            int counter = 0;
            while (rs2.next()) {
                String subAreaName = rs2.getString("name");
                int subAreaId = rs2.getInt("id");
                subAreas[counter][0] = subAreaName;
                subAreas[counter][1] = String.valueOf(subAreaId);
                counter++;
            }
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return subAreas;
    }

    public String[] signInUser(String username, String passsword) throws SQLException {

        //check if user exists in database and respond with id and admin value
        ResultSet resultSet = null;
        String[] userData = new String[2];

        String query = "SELECT id, admin FROM users WHERE username =? AND password =? LIMIT 1";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, passsword);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                userData[0] = String.valueOf(resultSet.getInt("id"));
                userData[1] = String.valueOf(resultSet.getInt("admin"));
            }

            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userData;
    }

    public ArrayList<MaterialType> getMaterialTypesFromSubAreaId(int subArea) {
        ArrayList<MaterialType> materialTypeArrayList = new ArrayList<>();
        ResultSet rs;
        String query = "SELECT mt.id, mtlt.sub_area_id, name FROM material_type mt " +
                "INNER JOIN material_type_link_table mtlt ON mtlt.material_type_id = mt.id " +
                "WHERE mtlt.sub_area_id = " + subArea;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int materialTypeId = rs.getInt("mt.id");
                int subAreaId = rs.getInt("mtlt.sub_area_id");
                String materialTypeName = rs.getString("mt.name");

                MaterialType materialType = new MaterialType(materialTypeId, subAreaId, materialTypeName);
                materialTypeArrayList.add(materialType);

            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return materialTypeArrayList;

    }

    public ArrayList<Configuration> getSavedConfigurations(int templateId) {
        ArrayList<Configuration> configurationArrayList = new ArrayList<>();
        ResultSet rs;
        String query = "SELECT name, total_embodied_energy, sci, default_config FROM saved_configurations " +
                "WHERE template_id = " + templateId;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                double total_embodied_energy = rs.getDouble("total_embodied_energy");
                double sci = rs.getDouble("sci");
                String name = rs.getString("name");
                int default_config = rs.getInt("default_config");

                Configuration configuration = new Configuration(name, total_embodied_energy, sci, default_config);
                configurationArrayList.add(configuration);

            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return configurationArrayList;

    }

    public ArrayList<Material> getMaterialsFromMaterialTypeId(int materialType) {
        ArrayList<Material> materialList = new ArrayList<>();
        ResultSet rs;
        String query = "SELECT m.id, mlt.material_type_id, m.name, m.unit, m.embodied_energy, m.embodied_co2, m.lifespan, m.releasability, m.image, m.stock, o.name,od.value FROM materials m  \n" +
                "INNER JOIN origin o ON o.id = m.origin\n" +
                "INNER JOIN origin_distance od ON od.id = m.origin_distance\n" +
                "INNER JOIN material_link_table mlt ON mlt.material_id = m.id\n" +
                "WHERE mlt.material_type_id = " + materialType +
                " ORDER BY m.embodied_energy DESC";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int materialId = rs.getInt("m.id");
                int materialTypeId = rs.getInt("mlt.material_type_id");
                String materialName = rs.getString("m.name");
                double materialUnit = rs.getDouble("m.unit");
                double materialEmbodiedEnergy = rs.getDouble("m.embodied_energy");
                String materialEmbodiedCo2 = rs.getString("m.embodied_co2");
                int materialLifeSpan = rs.getInt("m.lifespan");
                double materialReleasability = rs.getDouble("m.releasability");
                String materialImage = rs.getString("m.image");
                int materialStock = rs.getInt("m.stock");
                String materialOrigin = rs.getString("o.name");
                String materialOriginDistance = rs.getString("od.value");

                Material material = new Material(materialId, materialTypeId, materialName, materialOrigin, materialOriginDistance, materialUnit, materialEmbodiedEnergy, materialEmbodiedCo2, materialLifeSpan, materialReleasability, materialImage, materialStock);
                materialList.add(material);

            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return materialList;
    }

    public int registerUser(String username, String email, String first_name, String last_name, String password, int admin) throws SQLException {
        //insert the user into the databse and send back the response
        //response is used to show succes or failure, 1 means success, 0 means error
        String query = "INSERT INTO users (username, email, first_name, last_name, password, admin) VALUES (?, ?, ?, ?, ?, ?)";

        int sqlResponse = 0;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, first_name);
            preparedStatement.setString(4, last_name);
            preparedStatement.setString(5, password);
            preparedStatement.setInt(6, admin);

            int i = preparedStatement.executeUpdate();
            if (i > 0) {
                sqlResponse = i;
            } else {
                sqlResponse = i;
            }

            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sqlResponse;
    }

    public Material getMaterialsFromMaterialId(int materialId) {
        Material material = new Material();
        ResultSet rs;
        String query = "SELECT m.id, mlt.material_type_id, m.name, m.unit, m.embodied_energy, m.embodied_co2, m.lifespan, m.releasability, m.image, m.stock, o.name, od.value FROM materials m \n" +
                "INNER JOIN origin o ON o.id = m.origin \n" +
                "INNER JOIN origin_distance od ON od.id = m.origin_distance \n" +
                "INNER JOIN material_link_table mlt ON mlt.material_id = m.id \n" +
                "WHERE m.id = " + materialId;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int materialIdDb = rs.getInt("m.id");
                int materialTypeId = rs.getInt("mlt.material_type_id");
                String materialName = rs.getString("m.name");
                double materialUnit = rs.getDouble("m.unit");
                double materialEmbodiedEnergy = rs.getDouble("m.embodied_energy");
                String materialEmbodiedCo2 = rs.getString("m.embodied_co2");
                int materialLifeSpan = rs.getInt("m.lifespan");
                double materialReleasability = rs.getDouble("m.releasability");
                String materialImage = rs.getString("m.image");
                int materialStock = rs.getInt("m.stock");
                String materialOrigin = rs.getString("o.name");
                String materialOriginDistance = rs.getString("od.value");

                material.setMaterialId(materialIdDb);
                material.setMaterialTypeId(materialTypeId);
                material.setMaterialName(materialName);
                material.setMaterialUnit(materialUnit);
                material.setMaterialEmbodiedEnergy(materialEmbodiedEnergy);
                material.setMaterialEmbodiedCo2(materialEmbodiedCo2);
                material.setMaterialLifeSpan(materialLifeSpan);
                material.setMaterialReleasability(materialReleasability);
                material.setMaterialImage(materialImage);
                material.setMaterialStock(materialStock);
                material.setMaterialOrigin(materialOrigin);
                material.setMaterialOriginDistance(materialOriginDistance);
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return material;
    }

    public int insertMaterial(
            String material_type_name,
            int material_type_origin,
            int material_type_origin_distance,
            double material_type_unit,
            double material_type_embodied_energy,
            String material_type_embodied_co2,
            int material_type_lifespan,
            double material_type_releasability,
            String material_type_path_to_image,
            int material_type) {

        String insertmaterialsquery = "INSERT INTO materials (name, origin, origin_distance, unit, embodied_energy, embodied_co2, lifespan, releasability , image, stock) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        String insertmaterial_link_table = "INSERT INTO material_link_table (material_id, material_type_id) VALUES (LAST_INSERT_ID(), ?)";

        int sqlResponse = 0;
        //insert the material into the materials table and the material link table
        //return sql reponse, 1 for success 0 for error
        try {

            PreparedStatement preparedStatement2 = conn.prepareStatement(insertmaterialsquery);
            preparedStatement2.setString(1, material_type_name);
            preparedStatement2.setInt(2, material_type_origin);
            preparedStatement2.setInt(3, material_type_origin_distance);
            preparedStatement2.setDouble(4, material_type_unit);
            preparedStatement2.setDouble(5, material_type_embodied_energy);
            preparedStatement2.setString(6, material_type_embodied_co2);
            preparedStatement2.setInt(7, material_type_lifespan);
            preparedStatement2.setDouble(8, material_type_releasability);
            preparedStatement2.setString(9, material_type_path_to_image);
            preparedStatement2.setInt(10, 0);

            PreparedStatement preparedStatement3 = conn.prepareStatement(insertmaterial_link_table);
            preparedStatement3.setInt(1, material_type);

            sqlResponse = preparedStatement2.executeUpdate();
            preparedStatement3.executeUpdate();

            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sqlResponse;
    }

    public Object[] getMaterialData() {

        ResultSet rs = null;

        int materialamount = 0;
        int materialoriginamount = 0;
        int materialorigindistanceamount = 0;

        String query = "SELECT (SELECT COUNT(*) FROM material_type) AS count1,(SELECT COUNT(*) FROM origin) AS count2, (SELECT COUNT(*) FROM origin_distance) AS count3 FROM dual";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while(rs.next()) {
                materialamount = rs.getInt("count1");
                materialoriginamount = rs.getInt("count2");
                materialorigindistanceamount = rs.getInt("count3");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        String[][] material_types = new String[materialamount][2];
            String[][] material_origin  = new String[materialoriginamount][2];
            String[][] material_origin_distance  = new String[materialorigindistanceamount][2];


        ResultSet resultSet = null;

        String getmaterial_types = "SELECT name, id FROM material_type";
        String getmaterial_origin = "SELECT name, id FROM origin";
        String getmaterialorigin_distance = "SELECT value, id FROM origin_distance";

        try {

            PreparedStatement preparedStatement = conn.prepareStatement(getmaterial_types);
            resultSet = preparedStatement.executeQuery();

            int counter = 0;

            while(resultSet.next()) {
                String name = resultSet.getString("name");
                String id = String.valueOf(resultSet.getInt("id"));
                material_types[counter][0] = name;
                material_types[counter][1] = id;
                counter++;
            }

            PreparedStatement preparedStatement2 = conn.prepareStatement(getmaterial_origin);
            resultSet = preparedStatement2.executeQuery();

            counter = 0;

            while(resultSet.next()) {
                String name = resultSet.getString("name");
                String id = String.valueOf(resultSet.getInt("id"));
                material_origin[counter][0] = name;
                material_origin[counter][1] = id;
                counter++;
            }

            PreparedStatement preparedStatement3 = conn.prepareStatement(getmaterialorigin_distance);
            resultSet = preparedStatement3.executeQuery();

            counter = 0;

            while(resultSet.next()) {
                String value = resultSet.getString("value");
                String id = String.valueOf(resultSet.getInt("id"));
                material_origin_distance[counter][0] = value;
                material_origin_distance[counter][1] = id;
                counter++;
            }

            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Object[]{material_types, material_origin, material_origin_distance};
    }

    public int insertMaterialType(String materialType){
            String query = "INSERT INTO material_type (name) VALUES (?)";

            int sqlResponse = 0;

            try {
                PreparedStatement preparedStatement = conn.prepareStatement(query);
                preparedStatement.setString(1, materialType);

                sqlResponse = preparedStatement.executeUpdate();

                conn.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return sqlResponse;
    }

    public ArrayList<MaterialType> getAllMaterialTypes() {
        ArrayList<MaterialType> materialTypes = new ArrayList<>();

        ResultSet rs;
        String query = "SELECT id, name FROM material_type ORDER BY name";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int materialTypeId = rs.getInt("id");
                String materialTypeName = rs.getString("name");

                MaterialType materialType = new MaterialType();

                materialType.setMaterialTypeId(materialTypeId);
                materialType.setMaterialTypeName(materialTypeName);

                materialTypes.add(materialType);

            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return materialTypes;

    }

    public int insertTemplate(String template){
        String query = "INSERT INTO templates (name) VALUES (?)";
        int sqlResponse = 0;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, template);

            sqlResponse = preparedStatement.executeUpdate();

            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sqlResponse;
    }

    public int insertAreaFromTemplate(String areaName, String templateName){
        String query = "INSERT INTO areas (name, template_id) VALUES (?, (SELECT id FROM templates WHERE name = ? ORDER BY id DESC LIMIT 1))";
        int sqlResponse = 0;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, areaName);
            preparedStatement.setString(2, templateName);

            sqlResponse = preparedStatement.executeUpdate();

            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sqlResponse;
    }

    public int insertSubAreaFromArea(String subAreaName, String areaName, int sub){
        String query = "";
        if (sub == 1){
            query = "INSERT INTO sub_areas (name, sub_area_id) VALUES (?, (SELECT id FROM sub_areas sa WHERE sa.name = ? ORDER BY sa.id DESC LIMIT 1)) ";
        }else {
            query = "INSERT INTO sub_areas (name, area_id) VALUES (?, (SELECT id FROM areas a WHERE a.name = ? ORDER BY a.id DESC LIMIT 1))";
        }

        int sqlResponse = 0;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, subAreaName);
            preparedStatement.setString(2, areaName);

            sqlResponse = preparedStatement.executeUpdate();

            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sqlResponse;
    }

    public int insertMaterialTypeFromSubArea(String subAreaName, int materialType){
        String query = "INSERT INTO material_type_link_table (material_type_id, sub_area_id) VALUES (?, (SELECT id FROM sub_areas WHERE name = ? ORDER BY id DESC LIMIT 1))";
        int sqlResponse = 0;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, materialType);
            preparedStatement.setString(2, subAreaName);

            sqlResponse = preparedStatement.executeUpdate();

            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sqlResponse;
    }

    //Generate default template if it doesn't exist yet
    public void createDefaultTemplate(int templateId, int userId){
        String query = "SELECT id FROM saved_configurations WHERE template_id = ?";
        ResultSet rs;
        int counter = 0;

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, templateId);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                counter++;
            }

            if(counter == 0){
                query = "SELECT name FROM templates WHERE id = ?";
                preparedStatement = conn.prepareStatement(query);
                preparedStatement.setInt(1, templateId);
                rs = preparedStatement.executeQuery();

                String templateName = "";

                while (rs.next()) {
                    templateName = rs.getString("name");
                }

                templateName += " - Klassiek";

                //Count all subareas with materials from areas
                int areacount = 0;
                query = "SELECT COUNT(s.id) FROM templates t\n" +
                        "LEFT JOIN areas a ON a.template_id = t.id\n" +
                        "LEFT JOIN sub_areas s ON s.area_id = a.id\n" +
                        "LEFT JOIN sub_areas s2 on s2.sub_area_id = s.id\n" +
                        "LEFT JOIN sub_areas s3 ON s3.sub_area_id = s2.id\n" +
                        "LEFT JOIN sub_areas s4 ON s4.sub_area_id = s3.id\n" +
                        "LEFT JOIN sub_areas s5 ON s5.sub_area_id = s4.id\n" +
                        "WHERE t.id = ?";
                preparedStatement = conn.prepareStatement(query);
                preparedStatement.setInt(1, templateId);

                rs = preparedStatement.executeQuery();

                while(rs.next()){
                    areacount = rs.getInt(1);
                }

                //Get all subareas with materials from areas
                int[] subAreas = new int[areacount];
                query = "SELECT s.id, s2.id, s3.id, s4.id, s5.id FROM templates t\n" +
                        "LEFT JOIN areas a ON a.template_id = t.id\n" +
                        "LEFT JOIN sub_areas s ON s.area_id = a.id\n" +
                        "LEFT JOIN sub_areas s2 on s2.sub_area_id = s.id\n" +
                        "LEFT JOIN sub_areas s3 ON s3.sub_area_id = s2.id\n" +
                        "LEFT JOIN sub_areas s4 ON s4.sub_area_id = s3.id\n" +
                        "LEFT JOIN sub_areas s5 ON s5.sub_area_id = s4.id\n" +
                        "WHERE t.id = ?";
                preparedStatement = conn.prepareStatement(query);
                preparedStatement.setInt(1, templateId);

                rs = preparedStatement.executeQuery();

                int counter2 = 0;
                while (rs.next()){
                    int subArea1 = rs.getInt("s.id");
                    int subArea2 = rs.getInt("s2.id");
                    int subArea3 = rs.getInt("s3.id");
                    int subArea4 = rs.getInt("s4.id");
                    int subArea5 = rs.getInt("s5.id");

                    if(subArea5 != 0){
                        subAreas[counter2] = subArea5;
                    } else if (subArea4 != 0){
                        subAreas[counter2] = subArea4;
                    } else if (subArea3 != 0){
                        subAreas[counter2] = subArea3;
                    } else if (subArea2 != 0){
                        subAreas[counter2] = subArea2;
                    } else {
                        subAreas[counter2] = subArea1;
                    }

                    counter2++;
                }

                String subAreasString = Arrays.toString(subAreas).replace("[", "").replace("]", "");

                query = "SELECT MAX(m.embodied_energy) AS \"max\" FROM material_type_link_table mtlt \n" +
                        "JOIN material_type mt ON mt.id = mtlt.material_type_id\n" +
                        "JOIN material_link_table mlt ON mlt.material_type_id = mt.id\n" +
                        "JOIN materials m ON m.id = mlt.material_id\n" +
                        "\n" +
                        "WHERE mtlt.sub_area_id IN (" + subAreasString + ")\n" +
                        "GROUP BY mt.name";
                preparedStatement = conn.prepareStatement(query);

                rs = preparedStatement.executeQuery();

                int embodiedEnergy = 0;
                while(rs.next()){
                    embodiedEnergy += rs.getInt("max");
                }

                query = "INSERT INTO saved_configurations (name, template_id, total_embodied_energy, sci, default_config, user_id) VALUES (?,?,?,?,?,?)";
                preparedStatement = conn.prepareStatement(query);
                preparedStatement.setString(1, templateName);
                preparedStatement.setInt(2, templateId);
                preparedStatement.setDouble(3, (double) embodiedEnergy);
                preparedStatement.setDouble(4, 0.86);
                preparedStatement.setInt(5, 1);
                preparedStatement.setInt(6, userId);

                preparedStatement.executeUpdate();
            }

            conn.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}