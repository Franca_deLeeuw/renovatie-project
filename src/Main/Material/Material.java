package Main.Material;

public class Material {

   private int materialId;
   private int materialTypeId;
   private String materialName;
   private String materialOrigin;
   private String materialOriginDistance;
   private double materialUnit;
   private double materialEmbodiedEnergy;
   private String materialEmbodiedCo2;
   private int materialLifeSpan;
   private double materialReleasability;
   private String materialImage;
   private int materialStock;

    public Material(){

    }

    public Material(int materialId, int materialTypeId, String materialName, String materialOrigin, String materialOriginDistance, double materialUnit, double materialEmbodiedEnergy, String materialEmbodiedCo2, int materialLifeSpan, double materialReleasability, String materialImage, int materialStock) {
        this.materialId = materialId;
        this.materialTypeId = materialTypeId;
        this.materialName = materialName;
        this.materialOrigin = materialOrigin;
        this.materialOriginDistance = materialOriginDistance;
        this.materialUnit = materialUnit;
        this.materialEmbodiedEnergy = materialEmbodiedEnergy;
        this.materialEmbodiedCo2 = materialEmbodiedCo2;
        this.materialLifeSpan = materialLifeSpan;
        this.materialReleasability = materialReleasability;
        this.materialImage = materialImage;
        this.materialStock = materialStock;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public int getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(int materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialOrigin() {
        return materialOrigin;
    }

    public void setMaterialOrigin(String materialOrigin) {
        this.materialOrigin = materialOrigin;
    }

    public String getMaterialOriginDistance() {
        return materialOriginDistance;
    }

    public void setMaterialOriginDistance(String materialOriginDistance) {
        this.materialOriginDistance = materialOriginDistance;
    }

    public double getMaterialUnit() {
        return materialUnit;
    }

    public void setMaterialUnit(double materialUnit) {
        this.materialUnit = materialUnit;
    }

    public double getMaterialEmbodiedEnergy() {
        return materialEmbodiedEnergy;
    }

    public void setMaterialEmbodiedEnergy(double materialEmbodiedEnergy) {
        this.materialEmbodiedEnergy = materialEmbodiedEnergy;
    }

    public String getMaterialEmbodiedCo2() {
        return materialEmbodiedCo2;
    }

    public void setMaterialEmbodiedCo2(String materialEmbodiedCo2) {
        this.materialEmbodiedCo2 = materialEmbodiedCo2;
    }

    public int getMaterialLifeSpan() {
        return materialLifeSpan;
    }

    public void setMaterialLifeSpan(int materialLifeSpan) {
        this.materialLifeSpan = materialLifeSpan;
    }

    public double getMaterialReleasability() {
        return materialReleasability;
    }

    public void setMaterialReleasability(double materialReleasability) {
        this.materialReleasability = materialReleasability;
    }

    public String getMaterialImage() {
        return materialImage;
    }

    public void setMaterialImage(String materialImage) {
        this.materialImage = materialImage;
    }

    public int getMaterialStock() {
        return materialStock;
    }

    public void setMaterialStock(int materialStock) {
        this.materialStock = materialStock;
    }
}
