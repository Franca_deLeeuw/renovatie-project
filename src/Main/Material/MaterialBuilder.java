package Main.Material;

import Main.DatabaseHandler;

import java.util.ArrayList;

public class MaterialBuilder {

    public static ArrayList<Material> buildMaterialListFromMaterialTypeId(int materialTypeId) {
        ArrayList<Material> materialList = new ArrayList<>();
        DatabaseHandler databaseHandler = new DatabaseHandler();
        materialList = databaseHandler.getMaterialsFromMaterialTypeId(materialTypeId);

        return materialList;
    }

    public Material getMaterialsFromMaterialId(int materialId){
        DatabaseHandler databaseHandler = new DatabaseHandler();
        return databaseHandler.getMaterialsFromMaterialId(materialId);
    }
}
