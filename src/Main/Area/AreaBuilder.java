package Main.Area;
import Main.DatabaseHandler;
import Main.Material.Material;
import Main.Material.MaterialBuilder;
import Main.MaterialType.MaterialType;
import Main.MaterialType.MaterialTypeBuilder;

import java.util.ArrayList;

public class AreaBuilder {
    public String buildMenu(String template) {
        String areas= "";
        ArrayList<Area> areaList = new ArrayList<>();
        DatabaseHandler databaseHandler = new DatabaseHandler();

        areaList = databaseHandler.getAllAreas(Integer.valueOf(template));

        for (int i = 0; i<areaList.size(); i++) {
            areas = areas +
                    "<a href=\"#"+ areaList.get(i).areaName.replaceAll(" ", "") + "\" class=\"list-group-item list-group-item-action bg-light text-capitalize\">"+ areaList.get(i).areaName +"</a>\n";
        }

    return areas;
    }

    public String buildAllAreas(String template) {

        String areas= "";
        ArrayList<Area> areaList = new ArrayList<>();
        DatabaseHandler databaseHandler = new DatabaseHandler();

        areaList = databaseHandler.getAllAreas(Integer.valueOf(template));
        for (int i = 0; i<areaList.size(); i++) {
            areas = areas + "<div id=\""+ areaList.get(i).areaName.replaceAll(" ", "") + "\" class=\"container-fluid area\">\n" +
                            "<h1 class=\"mt-4 text-capitalize\">"+ areaList.get(i).areaName +"</h1>\n";
            String [][] subAreaList = areaList.get(i).getSubAreas(areaList.get(i).areaId, 0);
            for (int j = 0; j<subAreaList.length; j++) {

                areas = areas +
                        "<div id=\"subAreaAccordion"+subAreaList[j][1]+"\">\n" +
                        "   <div class=\"card\">\n" +
                        "       <div class=\"card-header\" id=\"subarea"+ subAreaList[j][1]+ "Title\">\n" +
                        "           <h5 class=\"mb-0\">\n" +
                        "               <button class=\"btn btn-link text-capitalize\" data-toggle=\"collapse\" data-target=\"#collapseSubArea"+subAreaList[j][1]+"\" aria-expanded=\"true\" aria-controls=\"collapseSubArea"+subAreaList[j][1]+"\">\n" +
                        subAreaList[j][0] +
                        "               </button>\n" +
                        "           </h5>\n" +
                        "       </div>\n" +
                        "       \n" +
                        "   <div id=\"collapseSubArea"+subAreaList[j][1]+"\" class=\"collapse\" aria-labelledby=\"subarea"+ subAreaList[j][1]+ "Title\" data-parent=\"#subAreaAccordion"+subAreaList[j][1]+"\">\n" +
                        "<div class=\"card-body\">\n";
                        String [][] subAreaList2 = areaList.get(i).getSubAreas(Integer.valueOf( subAreaList[j][1]), 1);
                        if(subAreaList2.length > 0){
                            for (int k = 0; k<subAreaList2.length; k++) {
                                areas = areas +
                                        "<div id=\"subAreaAccordion" + subAreaList2[k][1] + "\">\n" +
                                        "   <div class=\"card\">\n" +
                                        "       <div class=\"card-header\" id=\"subarea" + subAreaList2[k][1] + "Title\">\n" +
                                        "           <h5 class=\"mb-0\">\n" +
                                        "               <button class=\"btn btn-link text-capitalize\" data-toggle=\"collapse\" data-target=\"#collapseSubArea" + subAreaList2[k][1] + "\" aria-expanded=\"true\" aria-controls=\"collapseSubArea" + subAreaList2[k][1] + "\">\n" +
                                        subAreaList2[k][0] +
                                        "               </button>\n" +
                                        "           </h5>\n" +
                                        "       </div>\n" +
                                        "       \n" +
                                        "   <div id=\"collapseSubArea" + subAreaList2[k][1] + "\" class=\"collapse\" aria-labelledby=\"subarea" + subAreaList2[k][1] + "Title\" data-parent=\"#subAreaAccordion" + subAreaList2[k][1] + "\">\n" +
                                        "<div class=\"card-body\">\n";

                                String[][] subAreaList3 = areaList.get(i).getSubAreas(Integer.valueOf(subAreaList2[k][1]), 1);
                                if (subAreaList3.length > 0) {
                                    for (int l = 0; l < subAreaList3.length; l++) {
                                        areas = areas +
                                                "<div id=\"subAreaAccordion" + subAreaList3[l][1] + "\">\n" +
                                                "   <div class=\"card\">\n" +
                                                "       <div class=\"card-header\" id=\"subarea" + subAreaList3[l][1] + "Title\">\n" +
                                                "           <h5 class=\"mb-0\">\n" +
                                                "               <button class=\"btn btn-link text-capitalize\" data-toggle=\"collapse\" data-target=\"#collapseSubArea" + subAreaList3[l][1] + "\" aria-expanded=\"true\" aria-controls=\"collapseSubArea" + subAreaList3[l][1] + "\">\n" +
                                                subAreaList3[l][0] +
                                                "               </button>\n" +
                                                "           </h5>\n" +
                                                "       </div>\n" +
                                                "       \n" +
                                                "   <div id=\"collapseSubArea" + subAreaList3[l][1] + "\" class=\"collapse\" aria-labelledby=\"subarea" + subAreaList3[l][1] + "Title\" data-parent=\"#subAreaAccordion" + subAreaList3[l][1] + "\">\n" +
                                                "<div class=\"card-body\">\n";

                                                String[][] subAreaList4 = areaList.get(i).getSubAreas(Integer.valueOf(subAreaList3[l][1]), 1);
                                                if (subAreaList4.length > 0) {
                                                    for (int m = 0; m < subAreaList4.length; m++) {
                                                        areas = areas +
                                                                "<div id=\"subAreaAccordion" + subAreaList4[m][1] + "\">\n" +
                                                                "   <div class=\"card\">\n" +
                                                                "       <div class=\"card-header\" id=\"subarea" + subAreaList4[m][1] + "Title\">\n" +
                                                                "           <h5 class=\"mb-0\">\n" +
                                                                "               <button class=\"btn btn-link text-capitalize\" data-toggle=\"collapse\" data-target=\"#collapseSubArea" + subAreaList4[m][1] + "\" aria-expanded=\"true\" aria-controls=\"collapseSubArea" + subAreaList4[m][1] + "\">\n" +
                                                                subAreaList4[m][0] +
                                                                "               </button>\n" +
                                                                "           </h5>\n" +
                                                                "       </div>\n" +
                                                                "       \n" +
                                                                "   <div id=\"collapseSubArea" + subAreaList4[m][1] + "\" class=\"collapse\" aria-labelledby=\"subarea" + subAreaList4[m][1] + "Title\" data-parent=\"#subAreaAccordion" + subAreaList4[m][1] + "\">\n" +
                                                                "<div class=\"card-body\">\n";

                                                                MaterialTypeBuilder materialTypeBuilder = new MaterialTypeBuilder();
                                                                ArrayList<MaterialType> materialTypelist =  materialTypeBuilder.BuildMaterialTypeListFromSubAreaId(Integer.valueOf(subAreaList4[m][1]));
                                                                areas = areas + "<div class = \"row\">";
                                                                int counter = 1;
                                                                for (int n = 0; n<materialTypelist.size(); n++){

                                                                    ArrayList<Material>materialList = MaterialBuilder.buildMaterialListFromMaterialTypeId(materialTypelist.get(n).getMaterialTypeId());

                                                                    areas = areas + "<div class=\"col-sm\">\n" +
                                                                            "                                                                <div class=\"input-group mb-3\">\n" +
                                                                            "                                                                    <div id=\"imageFrame"+ materialTypelist.get(n).getMaterialTypeId() + "\" class=\"image-frame\">\n" +
                                                                            "                                                                        <img src=\"images/" + materialList.get(0).getMaterialImage() + "\" id='changeImage"+ materialTypelist.get(n).getMaterialTypeId() + "' />\n" +
                                                                            "                                                                           <div class=\"container\">"+
                                                                            "                                                                           <div class=\"row\">"+
                                                                            "                                                                           <div class=\"col-sm text-left\">"+
                                                                            "                                                                           <b>Oorsprong:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialOrigin()+
                                                                            "                                                                           </label></div></div>"+
                                                                            "                                                                           <div class=\"row\">"+
                                                                            "                                                                           <div class=\"col-sm text-left\">"+
                                                                            "                                                                           <b>Herkomst:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialOriginDistance()+
                                                                            "                                                                           </label></div></div>"+
                                                                            "                                                                           <div class=\"row\">"+
                                                                            "                                                                           <div class=\"col-sm text-left\">"+
                                                                            "                                                                           <b>Eenheid (kg/m2/aantal):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialUnit()+
                                                                            "                                                                           </label></div></div>"+
                                                                            "                                                                           <div class=\"row\">"+
                                                                            "                                                                           <div class=\"col-sm text-left\">"+
                                                                            "                                                                           <b>Embodied energy (Mj/kg):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialEmbodiedEnergy()+
                                                                            "                                                                           </label></div></div>"+
                                                                            "                                                                           <div class=\"row\">"+
                                                                            "                                                                           <div class=\"col-sm text-left\">"+
                                                                            "                                                                           <b>Embodied CO2 (kg CO2/kg):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialEmbodiedCo2()+
                                                                            "                                                                           </label></div></div>"+
                                                                            "                                                                           <div class=\"row\">"+
                                                                            "                                                                           <div class=\"col-sm text-left\">"+
                                                                            "                                                                           <b>Levensduur (jaar):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialLifeSpan()+
                                                                            "                                                                           </label></div></div>"+
                                                                            "                                                                           <div class=\"row\">"+
                                                                            "                                                                           <div class=\"col-sm text-left\">"+
                                                                            "                                                                           <b>Losmaakbaarheid:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialReleasability()+
                                                                            "                                                                           </label></div></div>"+
                                                                            "                                                                           <div class=\"row\">"+
                                                                            "                                                                           <div class=\"col-sm text-left\">"+
                                                                            "                                                                           <b>Voorraad:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialStock()+
                                                                            "                                                                           </label></div></div>"+
                                                                            "                                                                           </div>"+
                                                                            "                                                                    </div>\n" +
                                                                            "                                                                    <div class=\"input-group-prepend\">\n" +
                                                                            "                                                                        <label class=\"input-group-text text-capitalize\">"+ materialTypelist.get(n).getMaterialTypeName() + "</label>\n" +
                                                                            "                                                                    </div>\n" +
                                                                            "                                                                    <select class=\"text-capitalize custom-select\" id=\"itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "\" name=\"itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "\" onchange=\"showFormatImage('changeImage"+ materialTypelist.get(n).getMaterialTypeId() + "', 'itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "', 'imageFrame"+ materialTypelist.get(n).getMaterialTypeId() + "');\">\n" +
                                                                            "                                                                        <option\" data-img=\"" + materialList.get(0).getMaterialImage() +"\"value=\""+ materialList.get(0).getMaterialId() +"\"default\"" + materialList.get(0).getMaterialName()+ " " + materialList.get(0).getMaterialEmbodiedCo2() + "\"</option>\n";
                                                                    for (int o = 0; o<materialList.size(); o++){
                                                                        areas = areas + "<option class=\"text-capitalize\" data-img=\""+ materialList.get(o).getMaterialImage() +"\" value=\""+ materialList.get(o).getMaterialId() +"\">"+ materialList.get(o).getMaterialName() + "  (" + materialList.get(o).getMaterialEmbodiedCo2()+ " Embodied CO2/kg)" + "</option>\n";
                                                                    }
                                                                    areas = areas +

                                                                            "                                                                    </select>\n" +
                                                                            "                                                                </div>\n" +
                                                                            "                                                            </div>";
                                                                    if(counter == 2){
                                                                        counter = 1;
                                                                        areas = areas + "</div><div class = \"row\">";
                                                                    }else{
                                                                        counter++;
                                                                    }

                                                                }
                                                                areas = areas + "</div>";




                                                                areas = areas +
                                                                "</div>" +
                                                                "</div>" +
                                                                "</div>" +
                                                                "</div>";
                                                    }
                                                } else{
                                                    MaterialTypeBuilder materialTypeBuilder = new MaterialTypeBuilder();
                                                    ArrayList<MaterialType> materialTypelist =  materialTypeBuilder.BuildMaterialTypeListFromSubAreaId(Integer.valueOf(subAreaList3[l][1]));
                                                    areas = areas + "<div class = \"row\">";
                                                    int counter = 1;
                                                    for (int n = 0; n<materialTypelist.size(); n++){
                                                        ArrayList<Material>materialList = MaterialBuilder.buildMaterialListFromMaterialTypeId(materialTypelist.get(n).getMaterialTypeId());

                                                        areas = areas + "<div class=\"col-sm\">\n" +
                                                                "                                                                <div class=\"input-group mb-3\">\n" +
                                                                "                                                                    <div id=\"imageFrame"+ materialTypelist.get(n).getMaterialTypeId() + "\" class=\"image-frame\">\n" +
                                                                "                                                                        <img src=\"images/" + materialList.get(0).getMaterialImage() + "\" id='changeImage"+ materialTypelist.get(n).getMaterialTypeId() + "' />\n" +
                                                                "                                                                           <div class=\"container\">"+
                                                                "                                                                           <div class=\"row\">"+
                                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                                "                                                                           <b>Oorsprong:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialOrigin()+
                                                                "                                                                           </label></div></div>"+
                                                                "                                                                           <div class=\"row\">"+
                                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                                "                                                                           <b>Herkomst:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialOriginDistance()+
                                                                "                                                                           </label></div></div>"+
                                                                "                                                                           <div class=\"row\">"+
                                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                                "                                                                           <b>Eenheid (kg/m2/aantal):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialUnit()+
                                                                "                                                                           </label></div></div>"+
                                                                "                                                                           <div class=\"row\">"+
                                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                                "                                                                           <b>Embodied energy (Mj/kg):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialEmbodiedEnergy()+
                                                                "                                                                           </label></div></div>"+
                                                                "                                                                           <div class=\"row\">"+
                                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                                "                                                                           <b>Embodied CO2 (kg CO2/kg):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialEmbodiedCo2()+
                                                                "                                                                           </label></div></div>"+
                                                                "                                                                           <div class=\"row\">"+
                                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                                "                                                                           <b>Levensduur (jaar):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialLifeSpan()+
                                                                "                                                                           </label></div></div>"+
                                                                "                                                                           <div class=\"row\">"+
                                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                                "                                                                           <b>Losmaakbaarheid:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialReleasability()+
                                                                "                                                                           </label></div></div>"+
                                                                "                                                                           <div class=\"row\">"+
                                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                                "                                                                           <b>Voorraad:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialStock()+
                                                                "                                                                           </label></div></div>"+
                                                                "                                                                           </div>"+
                                                                "                                                                    </div>\n" +
                                                                "                                                                    <div class=\"input-group-prepend\">\n" +
                                                                "                                                                        <label class=\"input-group-text text-capitalize\">"+ materialTypelist.get(n).getMaterialTypeName() + "</label>\n" +
                                                                "                                                                    </div>\n" +
                                                                "                                                                    <select class=\"text-capitalize custom-select\" id=\"itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "\" name=\"itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "\" onchange=\"showFormatImage('changeImage"+ materialTypelist.get(n).getMaterialTypeId() + "', 'itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "', 'imageFrame"+ materialTypelist.get(n).getMaterialTypeId() + "');\">\n" +
                                                                "                                                                        <option\" data-img=\"" + materialList.get(0).getMaterialImage() +"\"value=\""+ materialList.get(0).getMaterialId() +"\"default\"" + materialList.get(0).getMaterialName()+ " " + materialList.get(0).getMaterialEmbodiedCo2() + "\"</option>\n";
                                                        for (int o = 0; o<materialList.size(); o++){
                                                            areas = areas + "<option class=\"text-capitalize\" data-img=\""+ materialList.get(o).getMaterialImage() +"\" value=\""+ materialList.get(o).getMaterialId() +"\">"+ materialList.get(o).getMaterialName() + "  (" + materialList.get(o).getMaterialEmbodiedCo2()+ " Embodied CO2/kg)" + "</option>\n";
                                                        } areas = areas +

                                                                "                                                                    </select>\n" +
                                                                "                                                                </div>\n" +
                                                                "                                                            </div>";
                                                        if(counter == 2){
                                                            counter = 1;
                                                            areas = areas + "</div><div class = \"row\">";
                                                        }else{
                                                            counter++;
                                                        }

                                                    }
                                                    areas = areas + "</div>";
                                                }

                                                areas = areas +
                                                "</div>" +
                                                "</div>" +
                                                "</div>" +
                                                "</div>";
                                    }

                                }else{
                                    MaterialTypeBuilder materialTypeBuilder = new MaterialTypeBuilder();
                                    ArrayList<MaterialType> materialTypelist =  materialTypeBuilder.BuildMaterialTypeListFromSubAreaId(Integer.valueOf(subAreaList2[k][1]));
                                    areas = areas + "<div class = \"row\">";
                                    int counter = 1;
                                    for (int n = 0; n<materialTypelist.size(); n++){
                                        ArrayList<Material>materialList = MaterialBuilder.buildMaterialListFromMaterialTypeId(materialTypelist.get(n).getMaterialTypeId());

                                        areas = areas + "<div class=\"col-sm\">\n" +
                                                "                                                                <div class=\"input-group mb-3\">\n" +
                                                "                                                                    <div id=\"imageFrame"+ materialTypelist.get(n).getMaterialTypeId() + "\" class=\"image-frame\">\n" +
                                                "                                                                        <img src=\"images/" + materialList.get(0).getMaterialImage() + "\" id='changeImage"+ materialTypelist.get(n).getMaterialTypeId() + "' />\n" +
                                                "                                                                           <div class=\"container\">"+
                                                "                                                                           <div class=\"row\">"+
                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                "                                                                           <b>Oorsprong:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialOrigin()+
                                                "                                                                           </label></div></div>"+
                                                "                                                                           <div class=\"row\">"+
                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                "                                                                           <b>Herkomst:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialOriginDistance()+
                                                "                                                                           </label></div></div>"+
                                                "                                                                           <div class=\"row\">"+
                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                "                                                                           <b>Eenheid (kg/m2/aantal):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialUnit()+
                                                "                                                                           </label></div></div>"+
                                                "                                                                           <div class=\"row\">"+
                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                "                                                                           <b>Embodied energy (Mj/kg):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialEmbodiedEnergy()+
                                                "                                                                           </label></div></div>"+
                                                "                                                                           <div class=\"row\">"+
                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                "                                                                           <b>Embodied CO2 (kg CO2/kg):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialEmbodiedCo2()+
                                                "                                                                           </label></div></div>"+
                                                "                                                                           <div class=\"row\">"+
                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                "                                                                           <b>Levensduur (jaar):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialLifeSpan()+
                                                "                                                                           </label></div></div>"+
                                                "                                                                           <div class=\"row\">"+
                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                "                                                                           <b>Losmaakbaarheid:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialReleasability()+
                                                "                                                                           </label></div></div>"+
                                                "                                                                           <div class=\"row\">"+
                                                "                                                                           <div class=\"col-sm text-left\">"+
                                                "                                                                           <b>Voorraad:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialStock()+
                                                "                                                                           </label></div></div>"+
                                                "                                                                           </div>"+
                                                "                                                                    </div>\n" +
                                                "                                                                    <div class=\"input-group-prepend\">\n" +
                                                "                                                                        <label class=\"input-group-text text-capitalize\">"+ materialTypelist.get(n).getMaterialTypeName() + "</label>\n" +
                                                "                                                                    </div>\n" +
                                                "                                                                    <select class=\"text-capitalize custom-select\" id=\"itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "\" name=\"itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "\" onchange=\"showFormatImage('changeImage"+ materialTypelist.get(n).getMaterialTypeId() + "', 'itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "', 'imageFrame"+ materialTypelist.get(n).getMaterialTypeId() + "');\">\n" +
                                                "                                                                        <option\" data-img=\"" + materialList.get(0).getMaterialImage() +"\"value=\""+ materialList.get(0).getMaterialId() +"\"default\"" + materialList.get(0).getMaterialName()+ " " + materialList.get(0).getMaterialEmbodiedCo2() + "\"</option>\n";
                                        for (int o = 0; o<materialList.size(); o++){
                                            areas = areas + "<option class=\"text-capitalize\" data-img=\""+ materialList.get(o).getMaterialImage() +"\" value=\""+ materialList.get(o).getMaterialId() +"\">"+ materialList.get(o).getMaterialName() + "  (" + materialList.get(o).getMaterialEmbodiedCo2()+ " Embodied CO2/kg)" + "</option>\n";
                                        } areas = areas +

                                                "                                                                    </select>\n" +
                                                "                                                                </div>\n" +
                                                "                                                            </div>";
                                        if(counter == 2){
                                            counter = 1;
                                            areas = areas + "</div><div class = \"row\">";
                                        }else{
                                            counter++;
                                        }

                                    }
                                    areas = areas + "</div>";
                                }
                                areas = areas +
                                        "</div>" +
                                        "</div>" +
                                        "</div>" +
                                        "</div>";
                            }
                        } else{
                            MaterialTypeBuilder materialTypeBuilder = new MaterialTypeBuilder();
                            ArrayList<MaterialType> materialTypelist =  materialTypeBuilder.BuildMaterialTypeListFromSubAreaId(Integer.valueOf(subAreaList[j][1]));
                            areas = areas + "<div class = \"row\">";
                            int counter = 1;
                            for (int n = 0; n<materialTypelist.size(); n++){
                                ArrayList<Material>materialList = MaterialBuilder.buildMaterialListFromMaterialTypeId(materialTypelist.get(n).getMaterialTypeId());

                                areas = areas + "<div class=\"col-sm\">\n" +
                                        "                                                                <div class=\"input-group mb-3\">\n" +
                                        "                                                                    <div id=\"imageFrame"+ materialTypelist.get(n).getMaterialTypeId() + "\" class=\"image-frame\">\n" +
                                        "                                                                        <img src=\"images/" + materialList.get(0).getMaterialImage() + "\" id='changeImage"+ materialTypelist.get(n).getMaterialTypeId() + "' />\n" +
                                        "                                                                           <div class=\"container\">"+
                                        "                                                                           <div class=\"row\">"+
                                        "                                                                           <div class=\"col-sm text-left\">"+
                                        "                                                                           <b>Oorsprong:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialOrigin()+
                                        "                                                                           </label></div></div>"+
                                        "                                                                           <div class=\"row\">"+
                                        "                                                                           <div class=\"col-sm text-left\">"+
                                        "                                                                           <b>Herkomst:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialOriginDistance()+
                                        "                                                                           </label></div></div>"+
                                        "                                                                           <div class=\"row\">"+
                                        "                                                                           <div class=\"col-sm text-left\">"+
                                        "                                                                           <b>Eenheid (kg/m2/aantal):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialUnit()+
                                        "                                                                           </label></div></div>"+
                                        "                                                                           <div class=\"row\">"+
                                        "                                                                           <div class=\"col-sm text-left\">"+
                                        "                                                                           <b>Embodied energy (Mj/kg):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialEmbodiedEnergy()+
                                        "                                                                           </label></div></div>"+
                                        "                                                                           <div class=\"row\">"+
                                        "                                                                           <div class=\"col-sm text-left\">"+
                                        "                                                                           <b>Embodied CO2 (kg CO2/kg):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialEmbodiedCo2()+
                                        "                                                                           </label></div></div>"+
                                        "                                                                           <div class=\"row\">"+
                                        "                                                                           <div class=\"col-sm text-left\">"+
                                        "                                                                           <b>Levensduur (jaar):</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialLifeSpan()+
                                        "                                                                           </label></div></div>"+
                                        "                                                                           <div class=\"row\">"+
                                        "                                                                           <div class=\"col-sm text-left\">"+
                                        "                                                                           <b>Losmaakbaarheid:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialReleasability()+
                                        "                                                                           </label></div></div>"+
                                        "                                                                           <div class=\"row\">"+
                                        "                                                                           <div class=\"col-sm text-left\">"+
                                        "                                                                           <b>Voorraad:</b> <label class='text-capitalize'>"+materialList.get(0).getMaterialStock()+
                                        "                                                                           </label></div></div>"+
                                        "                                                                           </div>"+
                                        "                                                                    </div>\n" +
                                        "                                                                    <div class=\"input-group-prepend\">\n" +
                                        "                                                                        <label class=\"input-group-text text-capitalize\">"+ materialTypelist.get(n).getMaterialTypeName() + "</label>\n" +
                                        "                                                                    </div>\n" +
                                        "                                                                    <select class=\"text-capitalize custom-select\" id=\"itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "\" name=\"itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "\" onchange=\"showFormatImage('changeImage"+ materialTypelist.get(n).getMaterialTypeId() + "', 'itemImage"+ materialTypelist.get(n).getMaterialTypeId() + "', 'imageFrame"+ materialTypelist.get(n).getMaterialTypeId() + "');\">\n" +
                                        "                                                                        <option\" data-img=\"" + materialList.get(0).getMaterialImage() +"\"value=\""+ materialList.get(0).getMaterialId() +"\"default\"" + materialList.get(0).getMaterialName()+ " " + materialList.get(0).getMaterialEmbodiedCo2() + "\"</option>\n";
                                for (int o = 0; o<materialList.size(); o++){
                                    areas = areas + "<option class=\"text-capitalize\" data-img=\""+ materialList.get(o).getMaterialImage() +"\" value=\""+ materialList.get(o).getMaterialId() +"\">"+ materialList.get(o).getMaterialName() + "  (" + materialList.get(o).getMaterialEmbodiedCo2()+ " Embodied CO2/kg)" + "</option>\n";
                                } areas = areas +
                                                                                                                
                                        "                                                                    </select>\n" +
                                        "                                                                </div>\n" +
                                        "                                                            </div>";
                                if(counter == 2){
                                    counter = 1;
                                    areas = areas + "</div><div class = \"row\">";
                                } else{
                                    counter++;
                                }


                            }
                            areas = areas + "</div>";

                        }

                        areas = areas +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
            }

            areas = areas +"</div>" +
                    " <div class=\"container position-fixed fixed-progress-bar\">\n" +
                    "                    <label class=\"text-center w-100 font-weight-bold test\">CO2 equiv balk t.o.v. klassiek concept</label>\n" +
                    "                    <div class=\"progress\">\n" +
                    "                        <div id=\"progressbar\" class=\"progress-bar progress-bar-striped progress-bar-animated\" role=\"progressbar\" style=\"width:100%\" ></div>\n" +
                    "                    </div>\n" +
                    "                </div>";

                    }
        return areas;
    }

    public int insertAreaFromTemplate(String areaName, String templateName){
        DatabaseHandler databaseHandler = new DatabaseHandler();
        return databaseHandler.insertAreaFromTemplate(areaName, templateName);
    }

}
