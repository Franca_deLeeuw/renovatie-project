package Main.Area;

import Main.DatabaseHandler;

public class Area {
    String areaName;
    int areaId;
    String[][] subAreas;
    String[][] items;

    public Area(String areaName, int areaId) {
        this.areaName = areaName;
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String[][] getSubAreas(int areaId,int sub) {
        DatabaseHandler databaseHandler = new DatabaseHandler();
        subAreas = databaseHandler.getSubAreasFromArea(areaId,sub);

        return subAreas;
    };
}
