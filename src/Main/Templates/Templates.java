package Main.Templates;

import Main.DatabaseHandler;

public class Templates {
    public String templateName;
    public int templateId;
    public String[][] areas;
    public String[] subAreas;

    public Templates(String templateName, int templateId) {
        this.templateName = templateName;
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String[][] getAreas(){
        DatabaseHandler databaseHandler = new DatabaseHandler();
        areas = databaseHandler.getAreasFromTemplate(templateId);

        return areas;
    };

    public String[] getSubAreas(int areaId){
        DatabaseHandler databaseHandler = new DatabaseHandler();
        subAreas = databaseHandler.getSubAreasFromTemplate(areaId);

        return subAreas;
    };

    public int getTemplateId()
    {
        return templateId;
    }
}