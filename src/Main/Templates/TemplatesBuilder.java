package Main.Templates;
import Main.Area.AreaBuilder;
import Main.DatabaseHandler;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class TemplatesBuilder {

    public String buildAllTemplates(){
        ArrayList<Templates> templates = new ArrayList<>();
        String templatesHtml = "";
        String templatesHtmlBlocks = "";
        String templatesHtmlModals = "";
        int counter = 0;

        DatabaseHandler databaseHandler = new DatabaseHandler();
        templates = databaseHandler.getAllTemplates();

        templatesHtmlBlocks = templatesHtmlBlocks + "<div class=\"row\">\n";

        //Loop though each template
        for (int i = 0; i < templates.size(); i++){
            counter++;

            String templateName = templates.get(i).templateName;
            int templateId = templates.get(i).templateId;
            String[][] areas = templates.get(i).getAreas();

            templatesHtmlBlocks = templatesHtmlBlocks +
                    "<a class=\"template-block text-capitalize\" data-toggle=\"modal\" data-target=\"#templateModal" + templateId + "\">\n" +
                    "   <div class=\"text-center icon-template\"><svg width=\"6em\" height=\"6em\" viewBox=\"0 0 16 16\" class=\"bi bi-house-door\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                    "  <path fill-rule=\"evenodd\" d=\"M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z\"/>\n" +
                    "  <path fill-rule=\"evenodd\" d=\"M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z\"/>\n" +
                    "</svg></div>" +
                    "   <div class=\"col-sm\">\n" +
                    "       <h4>" + templateName + "</h4>\n" +
                    "   </div>\n" +
                    "</a>\n";

            templatesHtmlModals = templatesHtmlModals +
                    "<div class=\"modal fade\" id=\"templateModal" + templateId + "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"templateModal" + templateId + "\" aria-hidden=\"true\">\n" +
                    "          <div class=\"modal-dialog modal-lg\" role=\"document\">\n" +
                    "                <div class=\"modal-content\">\n" +
                    "                    <div class=\"modal-header\">\n" +
                    "                        <h5 class=\"modal-title text-capitalize\" id=\"templateModal" + templateId + "Title\">" + templateName + "</h5>\n" +
                    "                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n" +
                    "                            <span aria-hidden=\"true\">&times;</span>\n" +
                    "                        </button>\n" +
                    "                    </div>\n" +
                    "                    <div class=\"modal-body\">\n" +
                    "                        <div class=\"row\">\n";

            for (int j = 0; j < areas.length; j++){
                templatesHtmlModals = templatesHtmlModals +
                        "<div class=\"col-sm mb-4\">\n" +
                        "   <h4 class=\"text-capitalize text-center\">" + areas[j][0] + "</h4>\n" +
                        "   <ul class=\"list-group \">\n";
                String[] subAreas = templates.get(i).getSubAreas(Integer.valueOf(areas[j][1]));
                for (int k = 0; k < subAreas.length; k++){
                    templatesHtmlModals = templatesHtmlModals +
                        "<li class=\"list-group-item text-capitalize\">" + subAreas[k] + "</li>\n";
                }

                templatesHtmlModals = templatesHtmlModals +
                        "   </ul>\n" +
                        "</div>\n";
            }

            templatesHtmlModals = templatesHtmlModals +
                "                        </div>\n" +
                "                    </div>\n" +
                "                    <div class=\"modal-footer\">\n" +
                "                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Annuleren</button>\n" +
                "                        <button type=\"button\" class=\"btn btn-primary\" onclick=\"setTemplate(" + templateId + ")\">Dit template gebruiken</button>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>";

            if (counter == 3) {
                templatesHtmlBlocks = templatesHtmlBlocks + "</div>\n<hr/>\n";
                templatesHtmlBlocks = templatesHtmlBlocks + "<div class=\"row\">\n";
                counter = 0;
            }

        };
        templatesHtmlBlocks = templatesHtmlBlocks + "</div>\n";
        templatesHtml = templatesHtmlBlocks + templatesHtmlModals;
        return templatesHtml;
    }

    public String insertTemplate(InputStreamReader isr){

        int error = 0;
        String response = "error";

        BufferedReader br = new BufferedReader(isr);

        int b;

        try {
            StringBuilder buf = new StringBuilder(512);
            while ((b = br.read()) != -1) {
                buf.append((char) b);
            }

            br.close();
            isr.close();

            JSONParser parser = new JSONParser();

            Object obj = parser.parse(buf.toString());
            JSONObject jsonObject = (JSONObject) obj;

            //Get template name from json
            String templateName = (String) jsonObject.get("Template");

            //Insert template name into database
            DatabaseHandler databaseHandler = new DatabaseHandler();
            int sqlresponse = databaseHandler.insertTemplate(templateName.replace("_", " "));

            if (sqlresponse != 1){
                error = 1;
            }

            //Get the amount of areas from json to be able to make an array
            Long areasAmount = (Long) jsonObject.get("Area amount");

            //Get areas from json
            JSONArray areas = (JSONArray) jsonObject.get("Areas");

            Iterator<JSONObject> iterator3 = areas.iterator();

            String[] mainAreas = new String[Math.toIntExact(areasAmount)];

            int counter = 0;
            while (iterator3.hasNext()) {
                String area = String.valueOf(iterator3.next());

                //Insert area to database
                AreaBuilder areaBuilder = new AreaBuilder();
                sqlresponse =  areaBuilder.insertAreaFromTemplate(area.replace("_", " "), templateName.replace("_", " "));
                if (sqlresponse != 1){
                    error = 1;
                }

                mainAreas[counter] = area;
                counter++;
            }

            //Get subareas from json
            JSONArray subareas = (JSONArray) jsonObject.get("Subareas");

            Iterator<JSONObject> iterator4 = subareas.iterator();
            while (iterator4.hasNext()) {
                JSONObject object = iterator4.next();
                String objectToString = object.toString();
                String sanitizedString = objectToString.replace("{", "").replace("}", "").replace("[", "").replace("]", "");
                String split[] = sanitizedString.split(":");
                String areaSanitized = split[0].replace("\"", "");
                String subareasSanitized = split[1].replace("\"", "");
                String[] subAreasSplit = subareasSanitized.split(",");

                //Chech if this is an subarea from an area or from a subarea
                int sub = 1;
                if (Arrays.asList(mainAreas).contains(areaSanitized)) {
                    sub = 0;
                }

                for(int i=0; i<subAreasSplit.length; i++){
                    //Insert subarea to database
                    DatabaseHandler databaseHandler1 = new DatabaseHandler();
                    sqlresponse = databaseHandler1.insertSubAreaFromArea(subAreasSplit[i].replace("_", " "), areaSanitized.replace("_", " "), sub);

                    if (sqlresponse != 1){
                        error = 1;
                    }
                }
            }


            //Get materials from json
            JSONArray materials = (JSONArray) jsonObject.get("Materials");

            Iterator<JSONObject> iterator5 = materials.iterator();
            while (iterator5.hasNext()) {
                JSONObject object = iterator5.next();
                String objectToString = object.toString();
                String sanitizedString = objectToString.replace("{", "").replace("}", "").replace("[", "").replace("]", "");
                String split[] = sanitizedString.split(":");
                String areaSanitized = split[0].replace("\"", "");
                String materialSanitized = split[1].replace("\"", "");
                String[] materialsSplit = materialSanitized.split(",");

                for(int i=0; i<materialsSplit.length; i++){
                    //Insert material to database
                    DatabaseHandler databaseHandler2 = new DatabaseHandler();
                    sqlresponse = databaseHandler2.insertMaterialTypeFromSubArea(areaSanitized.replace("_", " "), Integer.parseInt(materialsSplit[i]));
                    if (sqlresponse != 1){
                        error = 1;
                    }
                }
            }

            //Check is something along the way has thrown up an error
            if(error == 0) {
                response = "success";
            }else {
                response = "failed";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public void createDefaultTemplate(int templateId, int userId){
        DatabaseHandler databaseHandler = new DatabaseHandler();
        databaseHandler.createDefaultTemplate(templateId, userId);
    }

}
