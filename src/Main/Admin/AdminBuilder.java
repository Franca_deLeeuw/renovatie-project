package Main.Admin;

import Main.Area.Area;
import Main.DatabaseHandler;
import Main.Templates.Templates;

import java.util.ArrayList;
import java.util.Arrays;

public class AdminBuilder
{
    DatabaseHandler databaseHandler = new DatabaseHandler();
    public String getTemplates() {

        ArrayList<Templates> templates = databaseHandler.getAllTemplates();

        String html = "";


        for(int i = 0; i < templates.size(); i++) {
            html = html + "<option class=\"text-capitalize\" value='" + templates.get(i).getTemplateId() + "'>" + templates.get(i).getTemplateName() + "</option>";
        }

        return html;
}

public String getAreas(int templateId) {

    ArrayList<Area> areas = databaseHandler.getAllAreas(templateId);

    String html = "<option class=\"text-capitalize\"  value=\"\" selected disabled hidden>Kies woningbouwdeel</option>";


    for(int i = 0; i < areas.size(); i++) {
        html = html + "<option class=\"text-capitalize\"  value='" + areas.get(i).getAreaId() + "'>" + areas.get(i).getAreaName() + "</option>";
    }

    return html;
}

public String getSubAreasFromArea(int AreaId, int sub) {
        String[][] subAreas = databaseHandler.getSubAreasFromArea(AreaId, sub);
        System.out.println(Arrays.deepToString(subAreas));

        String html = "\"<option class=\\\"text-capitalize\\\"  value=\\\"\\\" selected disabled hidden>Kies woningbouwdeel functie</option>";

        for(int i = 0; i < subAreas.length; i++) {
            html = html + "<option class=\"text-capitalize\"  value='" + subAreas[i][1] + "'>" + subAreas[i][0] + "</option>";
        }

        return html;
}

    public String getMaterialData() {
        Object[] object = databaseHandler.getMaterialData();

        String[][] materialtypes = (String[][]) object[0];
        String[][] materialorigin = (String[][]) object[1];
        String[][] materialorigindistance = (String[][]) object[2];

        String html = "";

        for(int i = 0; i < materialtypes.length; i++) {
            html = html + "<option class=\"text-capitalize\"  value='" + materialtypes[i][1] + "'>" + materialtypes[i][0] + "</option>";
        }

        html = html+"~";

        for(int i = 0; i < materialorigin.length; i++) {
            html = html + "<option class=\"text-capitalize\"  value='" + materialorigin[i][1] + "'>" + materialorigin[i][0] + "</option>";
        }

        html = html+"~";

        for(int i = 0; i < materialorigindistance.length; i++) {
            html = html + "<option class=\"text-capitalize\"  value='" + materialorigindistance[i][1] + "'>" + materialorigindistance[i][0] + "</option>";
        }

        return html;
    }
}
