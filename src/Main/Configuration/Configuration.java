package Main.Configuration;

public class Configuration
{
    String name;
    double totalEmbodiedEnergy;
    double SCI;
    int defaultValue;

    public Configuration(String name, double totalEmbodiedEnergy, double SCI, int defaultValue)
    {
        this.name = name;
        this.totalEmbodiedEnergy = totalEmbodiedEnergy;
        this.SCI = SCI;
        this.defaultValue = defaultValue;
    }

    public String getName()
    {
        return name;
    }

    public double getTotalEmbodiedEnergy()
    {
        return totalEmbodiedEnergy;
    }

    public double getSCI()
    {
        return SCI;
    }

    public int getDefaultValue()
    {
        return defaultValue;
    }
}
