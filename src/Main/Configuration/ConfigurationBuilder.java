package Main.Configuration;

import Main.DatabaseHandler;

import java.util.ArrayList;

public class ConfigurationBuilder
{

    public String getSavedConfigurations(int templateId) {
        ArrayList<Configuration> configurationArrayList = new ArrayList<>();

        DatabaseHandler databaseHandler = new DatabaseHandler();
        configurationArrayList = databaseHandler.getSavedConfigurations(templateId);
        double progressBarMax = 0;
        String html = "<div class=\"container-fluid mt-3\">\n" +
                "                    <hr />\n" +
                "                    <h1>Totalen:</h1>\n" +
                "                    <table class=\"table\">\n" +
                "                        <thead>\n" +
                "                        <tr>\n" +
                "                            <th scope=\"col\"></th>\n" +
                "                            <th scope=\"col\">SCI:</th>\n" +
                "                            <th scope=\"col\">CO2:</th>\n" +
                "                        </tr>\n" +
                "                        </thead>\n" +
                "                        <tbody>\n" +
                "                        <tr style=\"background-color: rgba(0,0,0,.075);\">\n" +
                "                            <th style= \"width:50%\" scope=\"row\">Huidig Concept</th>\n" +
                "                            <td>0,86</td>\n" +
                "                            <td><div id=\"embodiedEnergy\">0</div> Kg. CO2 equiv</td>\n" +
                "                        </tr>\n";

                for(int i = 0; i < configurationArrayList.size(); i++) {
                    if(configurationArrayList.get(i).getDefaultValue() == 1) {
                        progressBarMax = configurationArrayList.get(i).getTotalEmbodiedEnergy();
                    }
                           html += "                        <tr>\n" +
                            "                            <th style= \"width:50%\" scope=\"row\">" + configurationArrayList.get(i).getName() + " </th>\n" +
                            "                            <td>" + configurationArrayList.get(i).getSCI() + "</td>\n" +
                            "                            <td>" + configurationArrayList.get(i).getTotalEmbodiedEnergy() + " Kg. CO2 equiv</td>\n" +
                            "                        </tr>\n";

                }


                html += "                        </tbody>\n" +
                        "                    </table>\n" +
                        "                </div>~" + progressBarMax;

        return html;
    }

}
